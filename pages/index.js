import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import styles from '../styles/Home.module.scss'
// import { getSortedPostsData } from '../lib/posts'
// import utilStyles from '../styles/Home.module.scss'
import Date from '../components/date'
import Link from 'next/link'
import fire from '../config/fire-conf';
import { useState, useEffect } from 'react';
import CreatePost from '../components/createPost'
import { Row } from 'react-bootstrap';
//import CustomCalendar from '../components/customCalendar';
import ViewProfile from '../components/ViewProfile';
import StaffInfo from '../components/ViewStaffInfo';
import ServiceBusinessCalendar from '../components/serviceBusinessCalendar';
import AddStaff from '../components/AddStaff';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Alert, { AlertProps } from '@material-ui/lab/Alert';
import { render } from 'react-dom';

const Home = () => {

  const [notification, setNotification] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);
  const [blogs, setBlogs] = useState([]);
  const [email, setUserEmail] = useState(null);
  const [photoURL, setUserPhotoURL] = useState(null);
  const [displayName, setDisplayName] = useState('');
  const [isSeller, setIsSeller] = useState(false);
  const [successLogOut, setSuccessfulLogOut] = useState(false);
  const [initialFetch, setInitialFetch] = useState(true);
  const [serviceBusiness, setServiceBusiness] = useState([]);
  const [serviceBusinessType, setServiceBusinessType] = useState(-1);
  const [serviceBusinessName, setServiceBusinessName] = useState('');
  // const [salonBusiness, setSalonBusiness] = useState([]);
  // const [salonBusinessType, setSalonBusinessType] = useState('');
  const [newStaffChanged, setNewStaffChanged] = useState(false);
  const [activeName, setActiveName] = useState(false);

  let msgs = ["a Reliable", "", "an Efficient", "", "a Robust", "", "a Brighter"];
  let classes = `${styles.agr_margin_left_10} ${styles.calendar_stylist_buttons}`
  const CONSTANTS = {
    DELETING_SPEED: 300,
    TYPING_SPEED: 200,
  }

  // console.log(email);
  fire.auth()
    .onAuthStateChanged((user) => {
      if (user) {
        setLoggedIn(true);
        //for setLogged In verify using email
        // console.log(user)
        // console.log(user.email);
        //for setLogged In verify using display Name
        // console.log(user.displayName);

        // console.log(user.photoURL);
        // for user with verified email // maybe only available for user who signed in with google
        // console.log(user.emailVerified);
        //assigned setUserEmail with value user.email 
        setUserEmail(user.email);
        if (user.photoURL) {
          setUserPhotoURL(user.photoURL);
        }
        if (user.displayName) {
          setDisplayName(user.displayName)
        }
      } else {
        setLoggedIn(false)
      }
    })

  useEffect(() => {
    if (email && loggedIn) {
      fire.firestore()
        .collection('userData')
        .doc(email)
        .get().then((snap) => {
          setIsSeller(snap.get('seller'));
          // console.log(snap.get('seller'));
        })
    }

  }, [email, loggedIn]) // What ever i want to find out in database , i put in the array

  useEffect(() => {
    if (loggedIn) {
      fire.firestore()
        .collection('salon')
        .get().then(snap => {
          //create a document immediately with the current contents of the single document , Then each time content change, it updates 
          //snap = query snapshot = result of a query , docs = using foreach method 
          if (snap.docs) {
            setServiceBusiness(snap.docs.map((doc, i) => ({
              index: i,
              name: doc.id,
            })));
            setInitialFetch(false);
          }
          else {
            setInitialFetch(false);

          }
        })
    }

  }, [loggedIn, newStaffChanged])
  // useEffect(()=>{
  //   if(activeName){
  //     classes += ` ${styles.Handsome}`;
  //   }
  // },[activeName])

  // console.log(serviceBusiness);

  const handleLogout = () => {

    fire.auth()
      .signOut()

    // .then(() => {
    //    setNotification('You have successfully Logged out');
    //    setTimeout(() => {
    //      setNotification('')
    //    }, 2000)
    // });

    setSuccessfulLogOut(true);
    // console.log(successLogOut)
  }

  const handleActive = (business) => {
    setServiceBusinessType(business.index)
    setServiceBusinessName(business.name)
    setActiveName(true)
    if (activeName && serviceBusinessType) {
      // console.log(business.index);
      // console.log('true');
      classes += ` ${styles.Handsome}`;
      // console.log(classes);
    }

    // console.log(classes);
    // console.log(business.index);
    // console.log(business.name);
    // console.log(activeName);
  }
  // if (activeName && serviceBusinessType) {
  //   console.log('true');
  //   classes += ` ${styles.Handsome}`;
  //   console.log(classes);
  // }
  // console.log(activeName);
  // console.log(serviceBusinessType);
  // console.log(classes);

  function TypeWriter({ messages, heading }) {
    const [state, setState] = useState({
      text: "",
      message: "",
      isDeleting: false,
      loopNum: 0,
      typingSpeed: CONSTANTS.TYPING_SPEED,
    });

    useEffect(() => {
      let timer = "";
      const handleType = () => {
        setState(cs => ({
          ...cs, // cs means currentState
          text: getCurrentText(cs),
          typingSpeed: getTypingSpeed(cs)
        }));
        timer = setTimeout(handleType, state.typingSpeed);
      };
      handleType();
      return () => clearTimeout(timer);
    }, [state.isDeleting]);

    useEffect(() => {
      if (!state.isDeleting && state.text === state.message) {
        setTimeout(() => {
          setState(cs => ({
            ...cs,
            isDeleting: true
          }))
        }, 1000);
      } else if (state.isDeleting && state.text === "") {
        setState(cs => ({
          ...cs, // cs means currentState
          isDeleting: false,
          loopNum: cs.loopNum + 1,
          message: getMessage(cs, messages)
        }));
      }
    }, [state.text, state.message, state.isDeleting, messages]);

    function getCurrentText(currentState) {
      return currentState.isDeleting
        ? currentState.message.substring(0, currentState.text.length - 1)
        : currentState.message.substring(0, currentState.text.length + 1);
    }

    function getMessage(currentState, data) {
      return data[Number(currentState.loopNum) % Number(data.length)];
    }

    function getTypingSpeed(currentState) {
      return currentState.isDeleting
        ? CONSTANTS.TYPING_SPEED
        : CONSTANTS.DELETING_SPEED;
    }

    return (
      <h3 className={`${styles.underline} ${styles.bolder} ${styles.letter_spacing}`}>
        {/* {heading}&nbsp; */}
        <span>{state.text}</span>
        <span id="cursor" className={`${styles.cursor}`} />
      </h3>
    );
  }

  return (
    // console.log(email),
    <div>
      <Head>
        <title>Appointment Scheduling and Reservation system</title>
        <link rel="shortcut icon" href="https://cdn.supersaas.net/favicon.ico"></link>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"></link>{/* Bootstrap class */}
      </Head>


      {notification}
      {!loggedIn
        ?

        // <div className={`${styles.responsive}`} >
        <div >
          <Layout />
          <div className={`${styles.fyp_landing_page_design} ${styles.landingPageZoom}`}>
            <div className={`container`} >
              {/* <div className={`${styles.fyp_flex_auto} ${styles.fyp_header_container} ${styles.cursor_pointer}`}> */}


              <div>
                <div className={`row ${styles.fyp_display_flex}`}>
                  <img src="https://cloud-reservation-nextjs.vercel.app/_next/image?url=%2Fimages%2Fcloud.gif&w=256&q=75" className={` ${styles.fyp_height_120_width_100}`}></img>
                  <div className={`${styles.fyp_align_self_center}  ${styles.fyp_padding_all_10}`}>
                    <h3 className={`${styles.fyp_flex_auto}`}>Welcome to Cloud Based Salon Reservation and Scheduling System</h3>
                    <hr className={`${styles.agr_margin_top_10}`}></hr>
                  </div>


                </div>
                <div className={`row ${styles.fyp_display_flex}`}>
                  {/* <div className={` ${styles.fyp_flex_auto}`}> */}
                  <div className={`col-lg-1`}>
                  </div>
                  {/* <div className={`${styles.fyp_flex_auto}`}> */}
                  <div className={`col-lg-11`}>
                    <h3 className={`${styles.fyp_destroy_margin_vertical}`}>Appointment Scheduling Software for Every Situation</h3>
                    <h3>Moving Towards{' '}
                      <strong>
                        <TypeWriter messages={msgs} />
                      </strong>
                      {' '}Future</h3>

                    <Link href="/users/Login_register">
                      <button className={`${styles.fyp_button_login} ${styles.fyp_flex_auto} ${styles.fyp_get_started_button} ${styles.enlargeHome}`}>
                        <span>Give It A Try &#8594;</span>
                      </button>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          {/* Text under the header text */}
          <div className={`container`}>
            <div>
              <h1 className={`${styles.what}`}>
                What? & About Us
              </h1>
            </div>
            <div className={`${styles.whatDesc} ${styles.whatDescPattern} ${styles.agr_padding_10_all}`}>
              Cloud Reservation is a platform which strives to connect sellers and clients in this vastly interconnected world.
              It bridges the cultural and communicative gaps often faced by small and large businesses alike.
            </div>
            <div>
              <h1 className={`${styles.what}`}>
                How to use this website?
              </h1>
            </div>
            <div className={`col-lg-12 ${styles.agr_margin_bottom_10}`} >
              <div className={`row ${styles.whatDescPattern} ${styles.fyp_padding_all_10} ${styles.fyp_destroy_padding_vertical}`}>
                <ul className={`nav nav-tabs`}>
                  <li className={`active`}><a data-toggle="tab" href="#Client">Client</a></li>
                  <li><a data-toggle="tab" href="#Stylist">Stylist/Owner</a></li>
                </ul>
                <hr className={`${styles.hrStyle}`}></hr>
                <div className={`tab-content`}>
                  <div id="Client" className={`tab-pane active`}>
                    <div className={styles.fyp_margin_3rem}>

                      <div className={`col-lg-3 col-xs-6 ${styles.agr_padding_20_all}`}>
                        <div className={`${styles.whatDesc}`}>
                          <i className={` glyphicon glyphicon-user`}></i>
                        </div>
                        <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                          Login / Register an account
                        </div>

                      </div>
                      <div className={`col-lg-3 col-xs-6 ${styles.agr_padding_20_all}`}>
                        <div className={`${styles.whatDesc}`}>
                          <i className={` glyphicon glyphicon-hand-up`}></i>
                        </div>
                        <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                          Click on the stylist name
                        </div>
                      </div>
                      <div className={`col-lg-3  col-xs-6 ${styles.agr_padding_20_all}`}>
                        <div className={`${styles.whatDesc}`}>
                          <i className={`glyphicon glyphicon-list-alt`}></i>
                        </div>
                        <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                          Make A Booking
                        </div>
                      </div>
                      <div className={`col-lg-3  col-xs-6 ${styles.agr_padding_20_all}`}>
                        <div className={`${styles.whatDesc}`}>
                          <i className={` glyphicon glyphicon-calendar`}></i>
                        </div>
                        <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                          Enjoy the website
                        </div>
                      </div>
                      <div className={`col-lg-12 col-xs-12 ${styles.fyp_margin_1rem}`}>
                        <h4>Looking for a service? Book A stylist with reasonable rates and flexible dates and time</h4>
                      </div>
                    </div>
                  </div>

                  <div id="Stylist" className={`tab-pane`}>
                    <div className={styles.fyp_margin_3rem}>

                      <div className={`col-lg-4 col-xs-4 ${styles.agr_padding_20_all}`}>
                        <div className={`${styles.whatDesc}`}>
                          <i className={` glyphicon glyphicon-user`}></i>
                        </div>
                        <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                          Login Into Assigned account
                        </div>

                      </div>
                      <div className={`col-lg-4 col-xs-4 ${styles.agr_padding_20_all}`}>
                        <div className={`${styles.whatDesc}`}>
                          <i className={` glyphicon glyphicon-hand-up`}></i>
                        </div>
                        <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                          View Client Appointment
                        </div>
                      </div>
                      <div className={`col-lg-4  col-xs-4 ${styles.agr_padding_20_all}`}>
                        <div className={`${styles.whatDesc}`}>
                          <i className={`glyphicon glyphicon-list-alt`}></i>
                        </div>
                        <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                          Have a great day ahead
                        </div>
                      </div>
                      <div className={`col-lg-12 col-xs-12 ${styles.fyp_margin_1rem}`}>
                        <h4>Are you skilled in Hair Styling? Be the stylist by contacting the stylist through <a href="tel:03-2222 1111"><h4>03-2222 1111</h4></a> </h4>
                      </div>
                    </div>
                  </div>
                </div>

              </div>

            </div>
            <footer className={`container ${styles.agr_margin_bottom_15}`}>
              <div>
                <div className={`col-lg-5 col-xs-12 ${styles.whatDescPattern}`}>
                  <h3 className={`${styles.what} ${styles.whatDescPattern} `}>Contact Info</h3>

                  <h4>Looking for a service? Book A stylist with reasonable rates and flexible dates and time</h4>
                  <h6 className={`${styles.whatDesc}`}>
                    {/* <div className={`${styles.whatDesc}`}> */}
                    <i className={`	glyphicon glyphicon-phone`}></i>{'  '}
                    {/* </div> */}
                    <a href="tel:03-2222 1111"><h4>03-2222 1111</h4></a>
                  </h6>
                  <h6 className={`${styles.whatDesc}`}>
                    <i className={`	glyphicon glyphicon-send`}></i>{'  '}
                    <a href="mailto:HairDo@gmail.com"><h4> HairDo@gmail.com</h4></a>
                  </h6>
                </div>
                <div className={`col-lg-2 col-xs-1  ${styles.agr_margin_bottom_15}`}></div>
                <div className={`col-lg-5 col-xs-12 ${styles.whatDescPattern} `}>
                  <h3 className={`${styles.what} ${styles.whatDescPattern}`}>Hair Do Mobile App</h3>

                  <h4>We also have HairDo App available, Do Download and Enjoy that is suitable for phones with multiple sizes </h4>
                  <div className={`${styles.fyp_text_align_center}`}>
                    <Link href="https://gitlab.com/ongandrew200/salon/-/blob/main/apk/app-debug.apk">
                      <button className={`${styles.fyp_button_login} ${styles.fyp_flex_auto} ${styles.fyp_get_started_button} ${styles.agr_margin_bottom_15} ${styles.enlargeHome}`}>
                        <span>Hair Do Mobile App &#8594;</span>
                      </button>
                    </Link>
                  </div>

                  <div>

                    <div className={`${styles.fyp_flex_auto}  ${styles.fyp_text_align_center} col-lg-5 col-xs-12 `}>
                      <img src='https://cdn.discordapp.com/attachments/750970916456103956/879303733954113586/Tablet.png'></img>
                      <h5>10 inch tablet</h5>
                    </div>
                    <div className={`col-lg-2 col-lg-1`}></div>
                    <div className={`${styles.fyp_flex_auto}  ${styles.fyp_text_align_center} col-lg-5 col-xs-12 `}>
                      <img src='https://cdn.discordapp.com/attachments/750970916456103956/879304416660975626/Phone.png'></img>
                      <h5>6.3 inch mobile phone</h5>
                    </div>
                  </div>
                </div>
              </div>
            </footer>

          </div>
        </div >

        :
        <div>

          <div className={`${styles.fyp_display_flex} ${styles.fyp_login_header}`}>

            <h1 className={`${styles.fyp_flex_auto} ${styles.alignItemMIddle} ${styles.fyp_text_align_center}`}>Welcome to Cloud Based Reservation and Scheduling System</h1>
            <div>
              {photoURL ?
                <span className={`${styles.profile_dropdown} ${styles.agr_margin_top_10} ${styles.agr_margin_right_10} ${styles.important}`}>
                  <img src={photoURL} className={`${styles.fyp_circle} ${styles.fyp_50} ${styles.dropdownbtn}`}></img>
                  <span className={`${styles.online_status}`}></span>
                  <div className={`${styles.dropdown_content}`}>
                    {/* <Link href="#" >
                      <a target="_blank" rel="noopener noreferrer" className={`${styles.fyp_display_flex} ${styles.agr_margin_between_row} ${styles.justify_content_center} ${styles.agr_margin_top_5} `}>Welcome, Mr/Mrs {displayName}</a>
                    </Link> */}
                    <div className={`${styles.fyp_display_flex}`}>
                      <i className={`glyphicon glyphicon-user ${styles.whatDesc} ${styles.black}`}></i>
                      <p className={`${styles.fyp_margin_top_auto} ${styles.agr_margin_between_row} ${styles.fyp_flex_auto} ${styles.fyp_text_align_center} ${styles.dropdown_content_header}`}>{displayName}</p>
                    </div>

                    {/* <p className={`${styles.fyp_margin_top_auto} ${styles.agr_margin_between_row} ${styles.fyp_flex_auto} ${styles.fyp_text_align_center} ${styles.dropdown_content_header}`}> Profile</p> */}
                    {displayName
                      ?
                      <>
                        <hr className={`${styles.hrStyle}`}></hr>
                        <Link href="https://gitlab.com/ongandrew200/dont-delete" >

                          <a className={`${styles.fyp_display_flex} ${styles.agr_margin_between_row} ${styles.justify_content_center} ${styles.agr_margin_top_5} `}>Git Lab Source Code</a>
                        </Link>
                      </>

                      :
                      <>
                      </>
                    }

                    <hr className={`${styles.hrStyle}`}></hr>
                    <a className={` ${styles.fyp_text_align_center}  ${styles.error}`}>
                      <div onClick={handleLogout}>Log out</div>

                    </a>
                  </div>
                </span>
                :
                <span className={`${styles.profile_dropdown} ${styles.agr_margin_top_10} ${styles.agr_margin_right_10} ${styles.important}`}>
                  <img src="https://w7.pngwing.com/pngs/696/429/png-transparent-computer-icons-user-account-colombo-filippetti-spa-human-icon-share-icon-artwork-user-account-thumbnail.png" className={`${styles.fyp_circle} ${styles.fyp_50}`}></img>
                  <span className={`${styles.online_status}`}></span>
                  <div className={`${styles.dropdown_content}`}>
                    {/* <p className={`${styles.fyp_margin_top_auto} ${styles.agr_margin_between_row} ${styles.fyp_flex_auto} ${styles.fyp_text_align_center} ${styles.dropdown_content_header}`}> Profile</p> */}
                    <Link href="https://gitlab.com/ongandrew200/dont-delete" >
                      <a target="_blank" rel="noopener noreferrer" className={`${styles.fyp_display_flex} ${styles.agr_margin_between_row} ${styles.justify_content_center} ${styles.agr_margin_top_5} `}>Git Lab Source Code</a>
                    </Link>
                    <hr className={`${styles.hrStyle}`}></hr>
                    <a className={` ${styles.fyp_text_align_center}  ${styles.error}`}>
                      <div onClick={handleLogout}>Log out</div>

                    </a>
                  </div>
                </span>
              }
            </div>


          </div>
          {/* <Link href="/">
            <a>Back</a>
          </Link> */}
          {/* JS Tabs */}
          <div className={`${styles.fyp_display_flex} ${styles.agr_margin_bottom_15} ${styles.agr_margin_top_15}`}>
            <div className={`${styles.fyp_js_tabs_border} ${styles.agr_margin_left_15} ${styles.minimizeHeader}`}>
              <ul className={`nav nav-tabs`}>
                <h4 className={`${styles.fyp_text_align_center} `}>Service Schedule Selection</h4>
                <li className={`active`}><a data-toggle="tab" href="#landing">About Us</a></li>
                <li><a data-toggle="tab" href="#calendar">Book Business</a></li>
                {isSeller
                  ?
                  <li>
                    <a data-toggle="tab" href="#configure">Staff Management</a>
                  </li>
                  :
                  <>
                  </>
                }
                <li>
                  <a data-toggle="tab" href="#visitor">View Profile</a>
                </li>
                {
                  isSeller
                    ?
                    <li>
                      <a data-toggle="tab" href="#staffInfo">View Staff Info</a>
                    </li>
                    :
                    <li>
                      <a data-toggle="tab" href="#staffInfo">View Stylist Speciality</a>
                    </li>

                }
                {/*<li><a data-toggle="tab" href="#menu3">Menu 3</a></li> */}
              </ul>
            </div>
            <div className={` ${styles.fyp_margin_1rem} ${styles.fyp_flex_auto} col-md-7 col-xs-12`}>
              {displayName
                ?
                <div className={`${styles.fyp_flex_auto} ${styles.fyp_text_align_center} ${styles.fyp_margin_side_6}`}>
                  <h3 className={`${styles.fyp_flex_auto}`}>Welcome , Mr/ Mrs {displayName}</h3>
                </div>
                : ' '
              }
            </div>
          </div>
          {/* <ul>
              {blogs.map(blog =>
                <li key={blog.id}>
                  <Link href="/blog/[id]" as={'/blog/' + blog.id}>
                    <a>{blog.title}</a>
                  </Link>
                </li>
              )}
            </ul> */}
          <div class="tab-content">
            <div id="landing" class="tab-pane active">
              <div className={`${styles.fyp_landing_page_design} col-lg-12 ${styles.landingPageZoom}`}>
                <div className={`container`} >
                  <div>
                    <div className={`row ${styles.fyp_display_flex}`}>
                      <img src="https://cloud-reservation-nextjs.vercel.app/_next/image?url=%2Fimages%2Fcloud.gif&w=256&q=75" className={` ${styles.fyp_height_120_width_100}`}></img>
                      <div className={`${styles.fyp_align_self_center}  ${styles.fyp_padding_all_10}`}>
                        <h3 className={`${styles.fyp_flex_auto}`}>Welcome to Cloud Based Salon Reservation and Scheduling System</h3>
                        <hr className={`${styles.agr_margin_top_10}`}></hr>
                      </div>


                    </div>
                    <div className={`row ${styles.fyp_display_flex}`}>
                      {/* <div className={` ${styles.fyp_flex_auto}`}> */}
                      <div className={`col-lg-1`}>
                      </div>
                      {/* <div className={`${styles.fyp_flex_auto}`}> */}
                      <div className={`col-lg-11`}>
                        <h3 className={`${styles.fyp_destroy_margin_vertical}`}>Appointment Scheduling Software for Every Situation</h3>
                        <h3>Moving Towards{' '}
                          <strong>
                            <TypeWriter messages={msgs} />
                          </strong>
                          {' '}Future</h3>

                        <Link href="/users/Login_register">
                          <button className={`${styles.fyp_button_login} ${styles.fyp_flex_auto} ${styles.fyp_get_started_button} ${styles.enlargeHome}`}>
                            <span>Give It A Try &#8594;</span>
                          </button>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
              <div className={`container col-lg-12`}>
                <div>
                  <h1 className={`${styles.what}`}>
                    What? & About Us
                  </h1>
                </div>
                <div className={`${styles.whatDesc} ${styles.whatDescPattern} ${styles.agr_padding_10_all}`}>
                  Cloud Reservation is a platform which strives to connect sellers and clients in this vastly interconnected world.
                  It bridges the cultural and communicative gaps often faced by small and large businesses alike.
                </div>
                <div>
                  <h1 className={`${styles.what}`}>
                    How to use this website?
                  </h1>
                </div>
                <div className={`col-lg-12 ${styles.agr_margin_bottom_10}`} >
                  <div className={`row ${styles.whatDescPattern} ${styles.fyp_padding_all_10} ${styles.fyp_destroy_padding_vertical}`}>
                    <ul className={`nav nav-tabs`}>
                      <li className={`active`}><a data-toggle="tab" href="#Client">Client</a></li>
                      <li><a data-toggle="tab" href="#Stylist">Stylist/Owner</a></li>
                    </ul>
                    <hr className={`${styles.hrStyle}`}></hr>
                    <div className={`tab-content`}>
                      <div id="Client" className={`tab-pane active`}>
                        <div className={styles.fyp_margin_3rem}>

                          <div className={`col-lg-3 col-xs-6 ${styles.agr_padding_20_all}`}>
                            <div className={`${styles.whatDesc}`}>
                              <i className={` glyphicon glyphicon-user`}></i>
                            </div>
                            <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                              Login / Register an account
                            </div>

                          </div>
                          <div className={`col-lg-3 col-xs-6 ${styles.agr_padding_20_all}`}>
                            <div className={`${styles.whatDesc}`}>
                              <i className={` glyphicon glyphicon-hand-up`}></i>
                            </div>
                            <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                              Click on the stylist name
                            </div>
                          </div>
                          <div className={`col-lg-3  col-xs-6 ${styles.agr_padding_20_all}`}>
                            <div className={`${styles.whatDesc}`}>
                              <i className={`glyphicon glyphicon-list-alt`}></i>
                            </div>
                            <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                              Make A Booking
                            </div>
                          </div>
                          <div className={`col-lg-3  col-xs-6 ${styles.agr_padding_20_all}`}>
                            <div className={`${styles.whatDesc}`}>
                              <i className={` glyphicon glyphicon-calendar`}></i>
                            </div>
                            <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                              Enjoy the website
                            </div>
                          </div>
                          <div className={`col-lg-12 col-xs-12 ${styles.fyp_margin_1rem}`}>
                            <h4>Looking for a service? Book A stylist with reasonable rates and flexible dates and time</h4>
                          </div>
                        </div>
                      </div>
                      <div id="Stylist" className={`tab-pane`}>
                        <div className={styles.fyp_margin_3rem}>

                          <div className={`col-lg-4 col-xs-4 ${styles.agr_padding_20_all}`}>
                            <div className={`${styles.whatDesc}`}>
                              <i className={` glyphicon glyphicon-user`}></i>
                            </div>
                            <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                              Login Into Assigned account
                            </div>

                          </div>
                          <div className={`col-lg-4 col-xs-4 ${styles.agr_padding_20_all}`}>
                            <div className={`${styles.whatDesc}`}>
                              <i className={` glyphicon glyphicon-hand-up`}></i>
                            </div>
                            <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                              View Client Appointment
                            </div>
                          </div>
                          <div className={`col-lg-4  col-xs-4 ${styles.agr_padding_20_all}`}>
                            <div className={`${styles.whatDesc}`}>
                              <i className={`glyphicon glyphicon-list-alt`}></i>
                            </div>
                            <div className={`${styles.How} ${styles.fyp_margin_1rem}`}>
                              Have a great day ahead
                            </div>
                          </div>
                          <div className={`col-lg-12 col-xs-12 ${styles.fyp_margin_1rem}`}>
                            <h4>Are you skilled in Hair Styling? Be the stylist by contacting the stylist through <a href="tel:03-2222 1111"><h4>03-2222 1111</h4></a> </h4>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>

                </div>
              </div>

              <footer className={`container ${styles.agr_margin_bottom_15}`}>
                <div>
                  <div className={`col-lg-5 col-xs-12 ${styles.whatDescPattern}`}>
                    <h3 className={`${styles.what} ${styles.whatDescPattern} `}>Contact Info</h3>

                    <h4>Looking for a service? Book A stylist with reasonable rates and flexible dates and time</h4>
                    <h6 className={`${styles.whatDesc}`}>
                      {/* <div className={`${styles.whatDesc}`}> */}
                      <i className={`	glyphicon glyphicon-phone`}></i>{'  '}
                      {/* </div> */}
                      <a href="tel:03-2222 1111"><h4>03-2222 1111</h4></a>
                    </h6>
                    <h6 className={`${styles.whatDesc}`}>
                      <i className={`	glyphicon glyphicon-send`}></i>{'  '}
                      <a href="mailto:HairDo@gmail.com"><h4> HairDo@gmail.com</h4></a>
                    </h6>
                  </div>
                  <div className={`col-lg-2 col-xs-1  ${styles.agr_margin_bottom_15}`}></div>
                  <div className={`col-lg-5 col-xs-12 ${styles.whatDescPattern} `}>
                    <h3 className={`${styles.what} ${styles.whatDescPattern}`}>Hair Do Mobile App</h3>

                    <h4>We also have HairDo App available, Do Download and Enjoy that is suitable for phones with multiple sizes </h4>
                    <div className={`${styles.fyp_text_align_center}`}>
                      <Link href="https://gitlab.com/ongandrew200/salon/-/blob/main/apk/app-debug.apk">
                        <button className={`${styles.fyp_button_login} ${styles.fyp_flex_auto} ${styles.fyp_get_started_button} ${styles.agr_margin_bottom_15} ${styles.enlargeHome}`}>
                          <span>Hair Do Mobile App &#8594;</span>
                        </button>
                      </Link>
                    </div>

                    <div>

                      <div className={`${styles.fyp_flex_auto}  ${styles.fyp_text_align_center} col-lg-5 col-xs-12 `}>
                        <img src='https://cdn.discordapp.com/attachments/750970916456103956/879303733954113586/Tablet.png'></img>
                        <h5>10 inch tablet</h5>
                      </div>
                      <div className={`col-lg-2 col-lg-1`}></div>
                      <div className={`${styles.fyp_flex_auto}  ${styles.fyp_text_align_center} col-lg-5 col-xs-12 `}>
                        <img src='https://cdn.discordapp.com/attachments/750970916456103956/879304416660975626/Phone.png'></img>
                        <h5>6.3 inch mobile phone</h5>
                      </div>
                    </div>
                  </div>
                </div>
              </footer>
            </div>
            <div id="calendar" class="tab-pane">
              <div className={`${styles.calendar_stylist} col-lg-12`}>
                <ul className={`${styles.agr_margin_vertical}`}>
                  {serviceBusiness.map((business) => (
                    // <li>{business.name}</li>
                    // <button onClick={() => { setServiceBusinessType(business.index) ,setServiceBusinessName(business.name) }} className={`${styles.agr_margin_left_10} ${styles.title}`}>
                    // <button onClick={() => handleActive(business)} className={`${styles.agr_margin_left_10} ${styles.calendar_stylist_buttons} ${activeName ? styles.Handsome : ''}`}>

                    <button onClick={() => handleActive(business)} className={classes}>
                      {business.name}
                    </button>


                  ))}
                </ul>
              </div>
              {/* {serviceBusinessType && <ServiceBusinessCalendar  businessType={serviceBusinessType} email={email} displayName={displayName} isSeller={isSeller} />} */}

              {serviceBusinessType > -1 && <ServiceBusinessCalendar serviceBusinessName={serviceBusinessName} serviceBusinessType={serviceBusinessType} email={email} displayName={displayName} isSeller={isSeller} />}
              {/* <CustomCalendar email={email} displayName={displayName} isSeller={isSeller} /> */}
            </div>
            <div id="configure" class="tab-pane fade">

              <div>
                <AddStaff newStaffHandler={() => {
                  setNewStaffChanged(!newStaffChanged)
                }} />
                {/* <AddStaff  /> */}

              </div>
            </div>

            <div id="visitor" class="tab-pane fade">

              <div>
                <ViewProfile photoURL={photoURL} displayName={displayName} email={email} isSeller={isSeller} />
              </div>
            </div>
            <div id="staffInfo" class="tab-pane fade">

              <div className={`container`}>
                <StaffInfo />
              </div>
            </div>
            {/* <div id="menu2" class="tab-pane fade">
              <h3>Menu 2</h3>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
            </div>
            <div id="menu3" class="tab-pane fade">
              <h3>Menu 3</h3>
              <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
            </div> */}
          </div>

        </div>


      }

      {loggedIn}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 200,
          height: 200,
          flexGrow: 0,
        }}
        open={successLogOut}
        onClose={() => {
          setSuccessfulLogOut(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          fontSize: '15px',
        }}
        >Successfully Log Out
        </Alert>
      </Snackbar>
      {/* { loggedIn && <CreatePost />} */}
      {/* very important line above */}
    </div >
  )
}
export default Home;