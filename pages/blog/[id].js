import Layout from '../../components/layout'
import Date from '../../components/date'
import utilStyles from '../../styles/Home.module.scss'
import fire from '../../config/fire-conf';
import Link from 'next/link'

const Blog = (props) => {

  return (  
    <div>
      <h2>{props.name}</h2>
      <p>
        {props.content}
      </p>
      <p>
        {props.num_people}
      </p>
     
      <Link href="/">
        <a>Back</a>
      </Link>
    </div>
  )
}

export const getServerSideProps = async ({ query }) => {
  const content = {}
  await fire.firestore()
    .collection('blog')
    .doc(query.id)
    .get()
    .then(result => {
      content['name'] = result.data().name;
      content['content'] = result.data().content;
      content['num_people'] = result.data().num_people;
      // content['scheduleDateTime'] = result.data().scheduleDateTime;
    });

  return {
    props: {
      name: content.name,
      content: content.content,
      num_people: content.num_people,
      // scheduleDateTime : content.scheduleDateTime,
    }
  }
}

export default Blog