// Next.js API route support: https://nextjs.org/docs/api-routes/introduction

//res = instance of a server response 
//req = instance of upcoming message 
export default (req, res) => {
  res.status(200).json({ name: 'John Doe' })
}
