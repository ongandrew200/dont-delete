import Link from 'next/link'
import styles from '../../styles/Home.module.scss'
import Layout from '../../components/layout'
import Login from '../../components/Login'
import Register from '../../components/Register'

const Login_Register = () => {

    return (

        <div>
            <Layout />
            <div className={`col-lg-4 col-s-1`}></div>
            <div className={`col-lg-4 col-s-10`}>
                <div className={`${styles.fyp_login_register}`}>
                    <div className={`row`}>

                        <ul className={`nav nav-tabs`}>
                            <li className={`active`}><a data-toggle="tab" href="#login">Login</a></li>
                            <li><a data-toggle="tab" href="#register">Register</a></li>
                            {/* <li><a data-toggle="tab" href="#menu2">Menu 2</a></li>
                            <li><a data-toggle="tab" href="#menu3">Menu 3</a></li> */}
                        </ul>
                        <div className={`tab-content`}>
                            <div id="login" className={`tab-pane active`}>
                                <Login />
                            </div>
                            <div id="register" className={`tab-pane fade`}>
                                <Register />
                            </div>

                        </div>
                        {/* <div className={`col-lg-6`}>
                            <Link href="/users/register">
                                <button className={` ${styles.fyp_button_login}`}>Register</button>
                            </Link>
                        </div>

                        <div className={`col-lg-6`}>
                            <Link href="/users/login">
                                {/* <button className={`col-lg-6 ${styles.fyp_button_login} ${styles.agr_margin_right_5}`}>Login</button>
                        <button className={`${styles.fyp_button_login}`}>Login</button>
                            </Link> 
                        </div> */}


                        {/* <div className={`${styles.fyp_flex_auto} ${styles.fyp_header_container} ${styles.cursor_pointer}`}> */}

                    </div>
                </div>

            </div >
            <div className={`col-lg-4 col-s-1`}></div>
        </div >
    )
}
export default Login_Register