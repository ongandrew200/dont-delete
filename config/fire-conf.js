// config/fire-config.js
import firebase from 'firebase';
import 'firebase/storage';

const firebaseConfig = {
    apiKey: "AIzaSyAt296Q2abGLR4TdzWtkDqyIfQoDW0CCho",
    authDomain: "laravelfirebase-34086.firebaseapp.com",
    databaseURL: "https://laravelfirebase-34086-default-rtdb.firebaseio.com",
    projectId: "laravelfirebase-34086",
    storageBucket: "laravelfirebase-34086.appspot.com",
    messagingSenderId: "303780915676",
    appId: "1:303780915676:web:9b77708b39c7af911a8719",
};

try {
    firebase.initializeApp(firebaseConfig);
    const storage = firebase.storage();
} catch (err) {
    if (!/already exists/.test(err.message)) {
        console.error('Firebase initialization error', err.stack)
    }
}
const fire = firebase;
const storage = firebase.storage();
export  {
    storage, fire as default
  }
 