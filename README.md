## Website address 
https://dont-delete.vercel.app/

## Language used 
React 

## How to duplicate the project and try it on your own 
1) git clone 
2) open visual studio code or any code editor software 
3) npm install 
4) npm run dev
5) Enjoy the website

## How to commit 
1) Git add .
2) Git commit -m "message"
3) Git push -u origin master

## Screenshot of the website

### Before Login 
![alt text](https://cdn.discordapp.com/attachments/881360432655388672/885132738917699634/unknown.png)

### After Login
![alt text](https://cdn.discordapp.com/attachments/881360432655388672/885131965181857802/unknown.png)
