import styles from '../styles/Home.module.scss'
import Head from 'next/head'
import Image from 'next/image'
import utilStyles from '../styles/Home.module.scss'
import Link from 'next/link'
import { useState } from 'react';
import Login_Register from '../pages/users/Login_register'

const name = 'Your Name'
export const siteTitle = 'Next.js Sample Website' //site title for other website

export default function Layout({ children, home }) {

  return (
    <div className={`${styles.layout_page} `}>
      <Head>
        <title>Appointment Scheduling and Reservation system</title>
        {/* <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"></link>Bootstrap class */}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react/16.13.1/umd/react.production.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/react-dom/16.13.1/umd/react-dom.production.min.js"></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"></link>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1"></meta>
      </Head>
      
      <div>
        <div className={`row ${styles.fyp_display_flex}`}>
          <div className={`${styles.fyp_flex_auto} ${styles.agr_width_eighty} ${styles.agr_margin_left_15}`}>
            <Link href="/">
              <div className={`${styles.cursor_pointer}`}>
                <img src="https://cdn.discordapp.com/attachments/750970916456103956/879307196247212063/HairDo.png" width='50px' height ='50px'></img>
                <p className={`${styles.agr_display_inline} ${styles.fyp_flex_auto}`}>Hair Do Salon</p>
              </div>
            </Link>
          </div>

          <div className={`${styles.fyp_flex_auto} ${styles.fyp_align_self_center}`}>
            <Link href="/users/Login_register">
              <button className={`${styles.fyp_button_login} ${styles.agr_margin_right_5}`}>Login/Register</button>
            </Link>
          </div>


          {/* <div className={`col-lg-2`}>
            <Link href="/users/register">
              <button className={`col-lg-5 ${styles.fyp_button_login} ${styles.agr_margin_right_5}`}>Register</button>
            </Link>


            {/* <div className={`${styles.fyp_flex_auto} ${styles.fyp_header_container} ${styles.cursor_pointer}`}> 
            <Link href="/users/login">
              <button className={`col-lg-5 ${styles.fyp_button_login} ${styles.agr_margin_right_5}`}>Login</button>
            </Link>
          </div> */}



        </div>
      </div>
    </div >

  )
}