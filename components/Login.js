import { useState, useEffect } from 'react';
import fire from '../config/fire-conf';
import { useRouter } from 'next/router'
import Link from 'next/link';
import styles from '../styles/Home.module.scss';
import GoogleLogin from 'react-google-login';
import { getAuth, getRedirectResult, GoogleAuthProvider, FacebookAuthProvider, signOut } from "firebase/auth";

export default function Login() {
  const [email, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [notify, setNotification] = useState('');
  const router = useRouter();
  const [blogs, setBlogs] = useState([]);

  const facebookProvider = new fire.auth.FacebookAuthProvider();
  const googleProvider = new fire.auth.GoogleAuthProvider();
  const [imageUrl, setImageUrl] = useState('');
  const [name, setName] = useState('');



  //for wrong input
  const handleLogin = (e) => {
    e.preventDefault();

    fire.auth()
      .signInWithEmailAndPassword(email, password)
      .catch((err) => {

        console.log(err.code, err.message)
        // setNotification(err.message)
        setNotification('The password or email is invalid. Going back to home screen')

        setTimeout(() => {
          setNotification('')
        }, 2000)
      })

    setUsername('')
    setPassword('')
    setTimeout(() => {
      router.push("/");

    }, 3000)
    // console.log(email)
    // router.push("/")
  }


  const responseGoogleSuccess = (response) => {

    console.log('Success');
    console.log(response.profileObj);
    setTimeout(() => {
      router.push("/");

    }, 3000)
    // console.log(email)

  }
  const responseGoogleFail = (response) => {

    // console.log('fail');
    // console.log(response);
  }

  //Google button without styling or custom button


  // useEffect(() => {
  //   fire.firestore()
  //     .collection('blog')
  //     .onSnapshot(snap => {
  //       const blogs = snap.docs.map(doc => ({
  //         id: doc.id,
  //         ...doc.data()
  //       }));
  //       setBlogs(blogs);
  //     })
  //     console.log(blogs);
  // }, []);
  return (
    <div>
      <div>

        {/* <div className={styles.backToHome}>
          <Link href="/">
            <a>← Back to home</a>
          </Link>
        </div> */}
     
          <h1 className={`${styles.fyp_text_align_center}`}>Login</h1>
     
        <div className={`${styles.fyp_display_flex}`}>
          <div className={`${styles.fyp_login} ${styles.fyp_flex_auto} ${styles.fyp_margin_side_15}`}>
            {notify}
            <form onSubmit={handleLogin}>
              <div className={`${styles.agr_margin_between_row}`}>
                <input className={`${styles.fyp_login_text}`} type="text" value={email} onChange={({ target }) => setUsername(target.value)} required placeholder="Email" />
              </div>
              <div className={`${styles.agr_margin_last_row}`}>
                <input className={`${styles.fyp_login_text}`} type="password" value={password} onChange={({ target }) => setPassword(target.value)} required placeholder="Password" />
              </div>
              {/* <div>
              {/* <input type="radio" id="seller" name="seller" value="other" />{' '}
              <label htmlFor="Seller">Seller</label>
              <br />
              <input type="radio" id="user" name="user" value="other" />{' '}
              <label htmlFor="User">User</label>
              <br /> */}
              {/* <input type="radio" id="RememberEmail" name="gender" value="other" />{' '}
              <label for="RememberEmail">Remember my email</label>
              <br />
              <input type="radio" id="AskedEveryTime" name="gender" value="other" />{' '}
              <label for="AskedEveryTime">Ask me every time </label>
              <br /> 
            </div> */}
              <div className={`${styles.login_register_button}`}>
                <button type="submit" className={`${styles.agr_margin_last_row}  ${styles.login_register_button}`}>
                  <img src="https://i.pinimg.com/originals/8f/c3/7b/8fc37b74b608a622588fbaa361485f32.png" alt="Facebook"></img>

                  <span>Login With Email and Password</span>
                </button>

              </div>
            </form>
            <div className={`${styles.login_register_button}`}>
              <hr />
              <button
                onClick={() => {
                  const facebookProvider = new fire.auth.FacebookAuthProvider();

                  fire.auth().signInWithPopup(facebookProvider);
                  // console.log(facebookProvider);
                  //sign up with Facebook
                  setTimeout(() => {
                    router.push("/")

                  }, 3000)
                }}
                className={`${styles.agr_margin_last_row} `}>
                <img src="https://p.kindpng.com/picc/s/736-7363083_download-facebook-logo-png-icon-high-quality-transparent.png" alt="Facebook" width='30px' height='30px'></img>
                {' '}<span>Sign in with Facebook</span>

              </button>
            </div>
            <div className={`${styles.login_register_button}`}>
              <button
                onClick={() => {
                  const googleProvider = new fire.auth.GoogleAuthProvider();

                  fire.auth().signInWithPopup(googleProvider);
                  // console.log(googleProvider);
                  //sign up with Google
                  setTimeout(() => {
                    router.push("/")

                  }, 3000)
                }}
                className={`${styles.agr_margin_last_row}`}>
                <img src="https://image.similarpng.com/thumbnail/2020/12/Colorful-google-logo-design-on-transparent-PNG-1.png" alt="Facebook" width='30px' height='30px'></img>

                <span>Sign in with Google</span>

              </button>
              {/* <GoogleLogin
              clientId="303780915676-lrgjad8csk2i519fc382fhdmd9janf59.apps.googleusercontent.com"
              buttonText="Login With Google"
              onSuccess={responseGoogleSuccess}
              onFailure={responseGoogleFail}
              cookiePolicy={'single_host_origin'}
            >
            </GoogleLogin> */}
            </div>
          </div>
        </div>
      </div>
    </div>


  )

}