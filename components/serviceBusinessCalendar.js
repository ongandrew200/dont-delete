import {
  Calendar,
  momentLocalizer,
  Views
} from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import fire from '../config/fire-conf';
import { useState, useEffect } from 'react';
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
// import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CustomEvent from './CustomEvent';
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import customStyles from '../styles/Home.module.scss';
import Typography from '@material-ui/core/Typography'
import { makeStyles, responsiveFontSizes } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Alert, { AlertProps } from '@material-ui/lab/Alert';
import emailjs from 'emailjs-com';

moment.locale("en-GB");
//momentLocalizer(moment);
const localizer = momentLocalizer(moment);
const DragAndDropCalendar = withDragAndDrop(Calendar)

//for react-big-calendar (month view)
const styles = {
  container: {
    // width: "80vw",
    height: "70vh",
    marginTop: "2em"
  }
};

const useStyles = makeStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
    fontSize: '20px',
  },
  formControl: {
    marginTop: theme.spacing(0),
    minWidth: 60,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
  textField: {
    fontWeight: 500,
    fontSize: '15px',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

const generateEventId = (event) => {
  return event.start.toLocaleString() + event.end.toLocaleString();
};

export default function ServiceBusinessCalendar({ serviceBusinessName, serviceBusinessType, email, displayName, isSeller }) {
  const updateEventNameInput = React.useRef(null);
  const updateEventNumPeopleInput = React.useRef(null);
  const updateEventContentInput = React.useRef(null);
  const updateEventCustEmailInput = React.useRef(null);

  const classes = useStyles();
  const [shops, setShops] = useState(null);
  const [shopData, setShopData] = useState(null);
  const templateId = 'template_zhb0oig';
  const websiteTitle = 'Appointment Scheduling and Reservation system';
  //initial state
  const [events, setEvents] = useState([]);
  const [orderConfirmedDetails, setOrderConfirmedDetails] = useState([]);
  const [content, setContent] = useState('');//description
  const [open, setOpen] = React.useState(false);//for add
  const [editOpen, setEditOpen] = React.useState(false);//for edit
  const [deleteOpen, setDeleteOpen] = React.useState(false);//for delete
  const [orderConfirmedOpen, setOrderConfirmedOpen] = React.useState(false);//for delete
  const [start, setStart] = useState(null);
  const [end, setEnd] = useState(null);
  const [custEmail, setCustEmail] = useState(email);
  const [name, setName] = useState('');//name
  const [num_people, setnumPeople] = useState(1);//numPeople // useState('')= string , (0) is for number and (true) is for boolean
  const [price, setPrice] = useState(10);
  const [computedPrice, setComputedPrice] = useState(0);
  const [draggedEvent, setDraggedEvent] = useState(null);
  const dragFromOutsideItem = () => {
    return draggedEvent;
  };
  const [isEdit, setEdit] = React.useState(false);//for editing mode  (boolean)
  const [clickedEvent, setClickedEvent] = useState({});
  const [initialFetch, setInitialFetch] = useState(true);
  const [snackbarEditSuccessOpen, setSnackbarEditSuccessOpen] = useState(false);
  const [snackbarEditOpen, setSnackbarEditOpen] = useState(false);
  const [snackbarAddOpen, setSnackbarAddOpen] = useState(false);
  const [snackbarDeleteOpen, setSnackbarDeleteOpen] = useState(false);

  const onEventDrop = ({
    event,
    start,
    end,
    isAllDay: droppedOnAllDaySlot
  }) => {
    if (!droppedOnAllDaySlot) {
      if (!event.disabled && !ifEventClashed({ start, end, id: event.id })) {
        const nextEvents = events.map((existingEvent) => {
          return existingEvent.id === event.id
            ? {
              ...existingEvent,
              id: generateEventId({ start, end }),
              start,
              end
            }
            : existingEvent;
        });
        setEvents(nextEvents);
      }
    }
  };
  const ifEventClashed = (event) => {
    for (const e of events) {
      if (event.id === e.id) {
        // console.log("same");
        continue;
      }
      if ((event.start < e.start && event.end > e.start) || (event.start >= e.start && event.start < e.end)) {
        // console.log(e);
        return true;
      }
    }
    return false;
  }
  // const datesAreOnSameDay = (date1, date2) => {
  //   return date1.getFullYear() === date2.getFullYear() &&
  //     date1.getMonth() === date2.getMonth() &&
  //     date1.getDate() === date2.getDate();
  // }
  const onEventResize = ({ event, start, end }) => {
    if (!event.disabled && !ifEventClashed({ start, end, id: event.id })) {
      const nextEvents = events.map((existingEvent) => {
        return existingEvent.id === event.id
          ? {
            ...existingEvent,
            id: generateEventId({ start, end }),
            start,
            end,
          }
          : existingEvent;
      });
      setEvents(nextEvents);
    }
  };
  const onDropFromOutside = ({ start, end }) => {
    const event = {
      id: generateEventId({ start, end }),
      title: draggedEvent.title,
      start,
      end,
      num_people,
    };
    setDraggedEvent(null);
    onEventDrop({ event, start, end });
  };
  const handleDragStart = (event) => {
    setDraggedEvent(event);
  };
  const calculateHours = (start, end) => {
    return ((end - start) / 36e5);
  }

  //to get data //for seller

  useEffect(() => {
    // console.log(isSeller);
    if (email) {
      fire.firestore()
        .collection('salon')
        .get().then(snap => {
          //create a document immediately with the current contents of the single document , Then each time content change, it updates 
          //snap = query snapshot = result of a query , docs = using foreach method 
          if (snap.docs) {

            // setShops(snap.docs.slice(1));
            setShops(snap.docs);
            // }))[0].events[0];

            // setEvents(shopData.slots[0].events)
            setInitialFetch(false);
          }
          else {
            setInitialFetch(false);

          }
        })
    }
    // console.log(events);
  }, [isSeller, email, serviceBusinessType])

  // console.log(shops);

  // console.log(events)
  useEffect(() => {
    // console.log(serviceBusinessName)
    // console.log(serviceBusinessType);
    // console.log(email);

    if (shops) {
      // console.log(shops);
      // console.log(shops[0].data());
      const shopDataTemp = shops

        .map(doc => ({
          // ownerEmail: doc.data().ownerEmail,
          slots: doc.data().email.map((e) => {
            // console.log(doc.id)
            // console.log(email)
            // console.log(e)
            const newE = {
              userEmail: e.userEmail,
              // itemID: e.itemID,
              events: e.events.map((event) => {
                return {
                  ...event,
                  disabled: isSeller ? false : e.userEmail != email,
                  start: new Date(event.start.toDate()),
                  end: new Date(event.end.toDate()),
                  userEmail: e.userEmail,
                  id: generateEventId(event),
                }
              })
            }
            // console.log(newE)
            return newE;
          }),

          shopName: doc.id,
          // }))[serviceBusinessType-1];
        }))[serviceBusinessType];
      if (shopDataTemp) {
        setEvents(shopDataTemp.slots.reduce((previous, user) => {
          return [
            ...previous,
            ...user.events,
          ]
        }, []))
        setShopData(shopDataTemp);
      }

    }

  }, [serviceBusinessType, shops])
  // console.log(events);

  useEffect(() => {
    if (shopData && events) {
      const temp = {

      }
      events.forEach((e) => {
        if (!(e.userEmail in temp)) {
          temp[e.userEmail] = []
        }
        temp[e.userEmail].push({
          start: e.start,
          end: e.end,
          title: e.title,
          content: e.content,
          num_people: e.num_people,
        });
      })
      // console.log(temp);


      const newData = Object.keys(temp).map((email) => {
        return {
          userEmail: email,
          events: temp[email]

        }
      })
      // console.log(newData);
      setShopData(newData);

      fire.firestore()
        .collection('salon')
        .doc(serviceBusinessName)
        .set(
          {
            email: newData,
          });
    }
  }, [events])


  // console.log(shopData);

  //to set for owner email , uncomment this 
  // .filter(doc => {
  //   console.log(doc.data().ownerEmail)
  //   return doc.data().ownerEmail === email;
  // })

  // //normal 
  // useEffect(() => {
  //   if (email && !isSeller) {
  //     // if (email) { //original code
  //     if (!initialFetch) {
  //       console.log(email)
  //       console.log(serviceBusinessName)
  //       fire.firestore()
  //         .collection('salon')
  //         .doc(serviceBusinessName)
  //         .set(
  //           {
  //           email : [{
  //             events: events.filter((e) => {
  //               return !e.disabled
  //             })
  //           }]
  //         });

  //     }

  //   }

  // }, [email, events, isSeller])

  const handleSubmit = (event) => {
    event.preventDefault();
    if (name && content && num_people) {
      if (!ifEventClashed({ start, end, id: event.id })) {
        setEvents([
          ...events,
          {
            id: generateEventId({ start, end }),
            start,
            end,
            title: name,
            content,
            num_people,
            userEmail: email,
          },
        ],

        )
        const templateParams =
        {
          // ...templateParams,
          name,
          content,
          num_people,
          start: new Date(start),
          startYear: start.getUTCFullYear(),
          startMonth: (start.getUTCMonth() + 1),
          startDay: start.getUTCDate(),
          startDate: start.toLocaleString('en-GB'),
          startHours: start.getHours(),
          startMinutes: ((start.getUTCMinutes() < 10 ? '0' : '') + start.getUTCMinutes()),
          custEmail,
          end: new Date(end),
          endDate: end.toLocaleString('en-GB'),
          endHours: end.getHours(),
          endMinutes: ((end.getUTCMinutes() < 10 ? '0' : '') + end.getUTCMinutes()),
          email,
          serviceBusinessName,
          websiteTitle
        };

        sendFeedback(templateParams);
        orderConfirmed(templateParams);
      }
      setName('');
      setContent('');
      setnumPeople(1);
      setOpen(false);
      setSnackbarAddOpen(true);
    }
    else {

      // setSnackbarOpen(true);
      setSnackbarEditOpen(true);
      ;
    }

  }
  /////////////////
  const sendFeedback = (templateParams) => {
    // console.log(templateId);
    // console.log(templateParams.custEmail);
    // console.log(templateParams);
    // console.log(templateParams.startDate);

    // console.log(templateParams.startMonth);

    // console.log(templateParams.startYear);
    // console.log(templateParams.endDate);
    // console.log(templateParams.startHours);
    // console.log(templateParams.startMinutes);
    // console.log(templateParams.endHours);

    // console.log(templateParams.endMinutes);
    emailjs.send('service_kfpwlh9', templateId, templateParams, 'user_2jDD3L1vuVQ2Mw9YcCWwT')
      .then(res => {
        // console.log('Email successfully Sent !')
      })
      // .catch(err => console.error('Oh well, you failed. Here some thoughts on the error that occured:', err))
  }
  const orderConfirmed = (orderConfirmedParams) => {
    setOrderConfirmedOpen(true);
    setOrderConfirmedDetails(orderConfirmedParams);

  }
  // ////////////
  const updateEvent = () => {
    if (updateEventNameInput.current.value && updateEventContentInput.current.value && updateEventNumPeopleInput.current.value) {
      const nextEvents = events.map((existingEvent) => {//new array next events //map = check all element then modify the one needed
        return existingEvent.id === clickedEvent.id //for each element if it check same date and 
          ? {
            ...existingEvent,
            id: generateEventId({ start: clickedEvent.start, end: clickedEvent.end }),
            title: updateEventNameInput.current.value,
            content: updateEventContentInput.current.value,
            num_people: updateEventNumPeopleInput.current.value,
          }
          : existingEvent;
      });
      setEvents(nextEvents);//replacing events with the value of nextEvents
      setEditOpen(false);
      setSnackbarEditSuccessOpen(true);
    }
    //add snackbar
    else {
      // setSnackbarOpen(true);
      setSnackbarEditOpen(true);
    }
  }


  const deleteEvent = () => {
    const nextEvents = events.filter((existingEvent) => {//new array next events //map = check all element then modify the one needed
      return existingEvent.id !== clickedEvent.id //for each element if it check same date and 

    });
    setEvents(nextEvents);//replacing events with the value of nextEvents
    setDeleteOpen(false);
    setEditOpen(false);
    setSnackbarDeleteOpen(true);
  }
  const handleOpen = () => {
    setOpen(true);
  }
  const handleClose = () => {
    setOpen(false);
  };
  const handleEditOpen = () => {
    setEditOpen(true);
  }
  const handleOrderConfirmedOpen = () => {
    setOrderConfirmedOpen(true);
  };
  const handleEditClose = () => {
    setEditOpen(false);
  };
  const handleDeleteOpen = () => {
    setDeleteOpen(true);
  }

  const handleDeleteClose = () => {
    setDeleteOpen(false);
  }

  const handleOrderConfirmedClose = () => {
    setOrderConfirmedOpen(false);
  };


  function EventAgenda({ event }) {
    return <div>
      <span>
        {!event.disabled && <>
          {/* <em style={{ opacity: 1, color: 'magenta',letterSpacing:'1px' }}> */}
          <em style={{ opacity: 1, letterSpacing: '1px' }}>

            <div>Name : {event.title} </div>
            <div>Service description : {event.content} </div>
            <div>Number of People : {event.num_people} </div>
            <div>Email : {email}</div>
          </em>
        </>}
      </span>
    </div>

  }
  return (
    // {email},
    // {serviceBusinessName},
    // console.log(email),
    <div style={styles.container} className={`col-lg-12`}>
      <div>
        <h3 className={`${customStyles.How}`}>
          Currently Viewing Stylist: {serviceBusinessName}
        </h3>
      </div>

      {/* <p>{email}</p> */}
      {/* <p>{displayName}</p> */}

      {/* Dialog Add Open */}
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add Schedule/ Reservation Time</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            id="name"
            margin="dense"
            type="text"
            onChange={({ target }) => setName(target.value)}
            fullWidth
            required
            className={classes.formTextSize}
            label="Name"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <TextField
            autoFocus
            id="Service Description"
            margin="dense"
            label="Service Description"
            type="text"
            onChange={({ target }) => setContent(target.value)}
            fullWidth
            required
            InputLabelProps={{
              className: classes.textField,
            }}

          /> */}
          <InputLabel id="Service Description">Service Description</InputLabel>
          <Select
            labelId="Service Description"
            displayEmpty
            value={content}
            onChange={({ target }) => setContent(target.value)}
            fullWidth
            InputLabelProps={{
              className: classes.textField,
            }}
          >
            <MenuItem value={'Hair Cut'}>Hair Cut</MenuItem>
            <MenuItem value={'Wash & Cut'}>Wash & Cut</MenuItem>
            <MenuItem value={'Fringe Trim'}>Fringe Trim</MenuItem>
            <MenuItem value={'Hair Setting'}>Hair Setting</MenuItem>
            <MenuItem value={'Hair Dye'}>Hair Dye</MenuItem>
            <MenuItem value={'Perm'}>Perm</MenuItem>
            <MenuItem value={'Rebonding'}>Rebonding</MenuItem>
            <MenuItem value={'Aroma Therapy'}>Aroma Therapy</MenuItem>
          </Select>
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates
            occasionally.
     </DialogContentText> */}
          <TextField
            autoFocus
            id="Num_People"
            margin="dense"
            label="Number of People"
            type="number"
            // onChange={({ target }) => setnumPeople(target.valueAsNumber)}
            value={num_people}
            onChange={({ target }) => setnumPeople(Math.max(1, Math.floor((target.valueAsNumber) ? Number(target.valueAsNumber) : target.valueAsNumber)))}
            InputProps={{ inputProps: { min: 1 } }}
            fullWidth
            required
            placeholder="1"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          <TextField
            autoFocus
            id="Customer_Email"
            margin="dense"
            type="email"
            defaultValue={email}
            fullWidth
            inputRef={updateEventCustEmailInput}
            onChange={({ target }) => setCustEmail(target.value)}
            required
            label="Customer Email"
            InputLabelProps={{
              className: classes.textField,
            }}

          />

          <TextField

            margin="dense"

            // onChange={({ target }) => setPrice(target.value)}
            fullWidth
            // required
            disabled={true}
            value={calculateHours(start, end)}
            label="Number of Hours"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          <TextField

            margin="dense"
            id="Price"
            // onChange={({ target }) => setPrice(target.value)}
            fullWidth
            // required
            disabled={true}
            value={price * calculateHours(start, end) * num_people}
            label="Price"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <p>{price * calculateHours(start,end)}</p> */}
        </DialogContent>

        <DialogActions>
          <Button variant="outlined" onClick={(event) => { handleSubmit(event) }} color="primary">
            Make Payment
          </Button>
        </DialogActions>
      </Dialog>

      {/* Dialog Edit Open */}
      <Dialog
        open={editOpen}
        onClose={handleEditClose}
        aria-labelledby="handleSubmit"

      >
        <DialogTitle id="handleSubmit">Edit Schedule/ Reservation Time</DialogTitle>
        <DialogContent>
          {/* <DialogContent> */}
          {/* <DialogContentText>
            <strong>Name</strong>
          </DialogContentText> */}
          {/* </DialogContent> */}
          {/* <InputLabel htmlFor="Name">Name</InputLabel> */}
          <TextField
            autoFocus
            id="Name"
            margin="dense"
            type="text"
            defaultValue={clickedEvent.title}
            fullWidth
            inputRef={updateEventNameInput}
            onChange={({ target }) => setName(target.value)}
            required
            label="Name"
            InputLabelProps={{
              className: classes.textField,
            }}

          />
          {/* <InputLabel htmlFor="content">Content</InputLabel> */}
          {/* <TextField
            autoFocus
            id="Service Description"
            margin="dense"
            type="text"
            defaultValue={clickedEvent.content}
            fullWidth
            required
            inputRef={updateEventContentInput}
            label="Service Description"
            InputLabelProps={{
              className: classes.textField,
            }}
          // onChange={({ target }) => setContent(target.value)}
          /> */}
          <InputLabel id="Service Description">Service Description</InputLabel>
          <Select
            labelId="Service Description"
            displayEmpty
            defaultValue={clickedEvent.content}
            inputRef={updateEventContentInput}
            fullWidth
            InputLabelProps={{
              className: classes.textField,
            }}
          >
            <MenuItem value={'Hair Cut'}>Hair Cut</MenuItem>
            <MenuItem value={'Wash & Cut'}>Wash & Cut</MenuItem>
            <MenuItem value={'Fringe Trim'}>Fringe Trim</MenuItem>
            <MenuItem value={'Hair Setting'}>Hair Setting</MenuItem>
            <MenuItem value={'Hair Dye'}>Hair Dye</MenuItem>
            <MenuItem value={'Perm'}>Perm</MenuItem>
            <MenuItem value={'Rebonding'}>Rebonding</MenuItem>
            <MenuItem value={'Aroma Therapy'}>Aroma Therapy</MenuItem>
          </Select>
          {/* <DialogContentText>
            <strong>Number of People</strong>
          </DialogContentText> */}
          {/* <InputLabel htmlFor="Num_people">Number of People</InputLabel> */}
          <TextField
            autoFocus
            id="Num_People"
            margin="dense"
            type="number"
            defaultValue={clickedEvent.num_people}
            value={num_people}
            InputProps={{ inputProps: { min: 1 } }} //input props for minimum value= 1 
            fullWidth
            required
            label="Number of People"
            inputRef={updateEventNumPeopleInput}
            onChange={({ target }) => setnumPeople(Math.max(1, Math.floor((target.valueAsNumber) ? Number(target.valueAsNumber) : target.valueAsNumber)))}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <InputLabel htmlFor="num_hours">Number of Hours</InputLabel> */}
          <TextField

            id="num_hours"
            margin="dense"
            // type="text"
            // onChange={({ target }) => setPrice(target.value)}
            fullWidth
            // required
            value={calculateHours(start, end)}
            //  defaultValue={price*calculateHours(start,end)}
            // variant="filled"
            disabled="true"
            label="Number of Hours"
            InputProps={{
              readOnly: true,
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <DialogContentText>
            <strong>Price</strong>
          </DialogContentText>
          <DialogContentText>
            Price
          </DialogContentText> */}
          {/* <InputLabel htmlFor="price">Price</InputLabel> */}
          <TextField

            id="price"
            margin="dense"
            // type="text"
            // onChange={({ target }) => setPrice(target.value)}
            fullWidth
            // required
            value={price * calculateHours(start, end) * num_people}
            //  defaultValue={price*calculateHours(start,end)}
            // variant="filled"
            disabled="true"
            label="Price"
            InputProps={{
              readOnly: true,
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <p>{price * calculateHours(start,end)}</p> */}
        </DialogContent>

        <DialogActions>
          <Button variant="outlined" onClick={() => { updateEvent() }} color="primary">
            Update Reservation
          </Button>
          <Button variant="outlined" onClick={handleDeleteOpen} className={customStyles.error}>
            Delete Reservation
          </Button>
        </DialogActions>

      </Dialog>

      {/* Dialog Order confirmed */}
      <Dialog
        open={orderConfirmedOpen}
        onClose={handleOrderConfirmedClose}
      >
        <DialogTitle id="handleSubmit">Appointment has been Scheduled, See you Soon !</DialogTitle>

        <DialogContent>
          <TextField
            label="Name"
            margin="dense"
            fullWidth
            defaultValue={orderConfirmedDetails.name}
            InputLabelProps={{
              className: classes.textField,

            }}
            InputProps={{
              readOnly: true,
            }}

          >
          </TextField>
          {/* </DialogContent>
        <DialogContent> */}

          <TextField
            label="Service Description"
            margin="dense"
            defaultValue={orderConfirmedDetails.content}
            fullWidth
            type="textfield"
            InputLabelProps={{
              className: classes.textField,

            }}
            InputProps={{
              readOnly: true,
            }}

          >
          </TextField>
          {/* </DialogContent>
        <DialogContent> */}
          <TextField
            label="Number Of People"
            margin="dense"
            fullWidth
            defaultValue={orderConfirmedDetails.num_people}
            InputLabelProps={{
              className: classes.textField,

            }}
            InputProps={{
              readOnly: true,
            }}

          >
          </TextField>
          {/* </DialogContent>
        <DialogContent> */}
          <TextField
            label="Date And Time"
            margin="dense"
            fullWidth
            defaultValue={orderConfirmedDetails.startDate}
            InputLabelProps={{
              className: classes.textField,

            }}
            InputProps={{
              readOnly: true,
            }}

          >
          </TextField>
          <TextField
            label="Customer Email"
            margin="dense"
            fullWidth
            defaultValue={orderConfirmedDetails.custEmail}
            InputLabelProps={{
              className: classes.textField,

            }}
            InputProps={{
              readOnly: true,
            }}

          >
          </TextField>
        </DialogContent>

        <Button variant="outlined" onClick={handleOrderConfirmedClose} color="primary">
          OK, Thank you very much
        </Button>



      </Dialog>

      {/* Dialog Delete confirmation */}
      <Dialog
        open={deleteOpen}
        onClose={handleDeleteClose}
        aria-labelledby="handleSubmit"
      // fullWidth={fullWidth}
      // maxWidth= {maxWidth}
      >
        <DialogTitle>Are you sure you want to delete?</DialogTitle>
        <DialogActions>
          <Button variant="outlined" onClick={() => { deleteEvent() }} color="primary">
            Yes
          </Button>
          <Button variant="outlined" onClick={handleDeleteClose} className={customStyles.error}>
            No
          </Button>
        </DialogActions>
      </Dialog>

      <DragAndDropCalendar
        selectable="ignoreEvents"
        localizer={localizer}
        onEventDrop={onEventDrop}
        onEventResize={onEventResize}
        dragFromOutsideItem={dragFromOutsideItem}
        onDropFromOutside={onDropFromOutside}
        handleDragStart={handleDragStart}
        resizable
        events={events}
        defaultView={Views.WEEK}
        views={[Views.DAY, Views.WEEK, Views.MONTH, Views.AGENDA]}
        steps={60}
        // defaultDate={new Date(2021, 0, 30)} // January is 0 , december is 11 // being counted like that
        // onSelectEvent={events =>alert(events.title) }
        onSelectEvent={event => {
          // updateEvent(events);
          if (event.disabled) {
            return;
          }
          handleEditOpen();
          // handleDeleteOpen();
          // updateEvent(event);//to check for console log
          setClickedEvent(event);
          setnumPeople(event.num_people);
          setStart(event.start);
          setEnd(event.end);
          // console.log(events.title);
        }
        }
        onSelectSlot={({ start, end, num_people }) => {
          handleOpen();
          // console.log(open);
          setStart(start);
          setEnd(end);
        }}
        components={{
          event: CustomEvent,
          agenda: {
            event: EventAgenda
          }
        }}
        timeslots={2}//custom time grid for timeslots , 
        min={new Date(2021, 0, 27, 10, 0, 0)}
        max={new Date(2021, 0, 27, 22, 0, 0)}
        eventPropGetter={(event) => {
          return {
            style: {
              backgroundColor: event.disabled ? 'grey' : '#38aec1',
              opacity: event.disabled ? '0.3' : '0.8',
              cursor: event.disabled ? 'no-drop' : 'grab',
              color: event.disabled ? '' : 'white',
            }

          }
        }}


      />
      {/*Snack Bar Edit */}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 300,
          height: 200,
          flexGrow: 0,
        }}
        open={snackbarEditOpen}
        onClose={() => {
          setSnackbarEditOpen(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          fontSize: '15px',
        }}
          severity="warning"
        >Please fill in all details
        </Alert>
      </Snackbar>
      {/* Snackbar Delete */}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 200,
          height: 200,
          flexGrow: 0,
        }}
        open={snackbarDeleteOpen}
        onClose={() => {
          setSnackbarDeleteOpen(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          fontSize: '15px',
        }}
          severity="warning"
        >Successfully Deleted
        </Alert>
      </Snackbar>
      {/* Snackbar Add */}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 200,
          height: 200,
          flexGrow: 0,
        }}
        open={snackbarAddOpen}
        onClose={() => {
          setSnackbarAddOpen(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          fontSize: '15px',
        }}
        >Successfully Added, Confirmation Email sent !
        </Alert>
      </Snackbar>
      {/* Snackbar Edit Success */}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 200,
          height: 200,
          flexGrow: 0,
        }}
        open={snackbarEditSuccessOpen}
        onClose={() => {
          setSnackbarEditSuccessOpen(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          fontSize: '15px',
        }}
        >Successfully edited
        </Alert>
      </Snackbar>
    </div>
  );
}
