import customStyles from '../styles/Home.module.scss';
import { Table } from 'react-bootstrap'
export default function StaffInfo() {
    const styles = {
        container: {
        //   width: "90vw",
        //   height: "70vh",
          margin: "1em",
          overflow:"auto",
        }
      };
    return (
        <div style={styles.container} className={`col-lg-12`}>
            <div className={`${customStyles.fyp_display_flex} ${customStyles.agr_margin_top_6}`}>
                <div>
                    <Table striped bordered hover responsive="xl" className={`${customStyles.fyp_text_align_center} ${customStyles.agr_vertical_mid}`}>
                        <thead>
                            <tr>
                                <th className={`${customStyles.fyp_text_align_center}`}>Photo</th>
                                <th className={`${customStyles.fyp_text_align_center}`}>Name</th>
                                <th className={`${customStyles.fyp_text_align_center}`}>Speciality</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <img src='https://lh3.googleusercontent.com/a-/AOh14GiqaFliM4ZfgcgCo7nJOtQtvv8_o8FsxtxVYvx8wXk=s96-c' className={`${customStyles.fyp_circle} ${customStyles.fyp_100} ${customStyles.dropdownbtn}`}></img>
                                </td>
                                <td className={`${customStyles.agr_vertical_mid}`}>Andrew</td>

                                <td className={`${customStyles.agr_vertical_mid} col-lg-6`}>
                                    <div>
                                        Founder of this Hair Do Salon. Very Good Salon Service ,Especially in performing haircuts as per customer request
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src='https://pyxis.nymag.com/v1/imgs/05e/f72/3bd7aee4684405900fcbb75907f233ae61-12-zac-efron.rsquare.w1200.jpg' className={`${customStyles.fyp_circle} ${customStyles.fyp_100} ${customStyles.dropdownbtn}`}></img>
                                </td>
                                <td className={`${customStyles.agr_vertical_mid}`}>Cheng HY</td>
                                <td className={`${customStyles.agr_vertical_mid} col-lg-6`}>
                                    <div>
                                        Co-founder of Hair Do Salon. Achieved Multiple Awards from world renowned competition for crazy wonderful sense of hairstyling . Cheng is the Kamar Taj 2019 Champion and Sanctum Sanctorum 2018 World Tour Champion
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src='https://cdn.discordapp.com/attachments/750970916456103956/881278944752594975/unknown.png' className={`${customStyles.fyp_circle} ${customStyles.fyp_100} ${customStyles.dropdownbtn}`}></img>
                                </td>
                                <td className={`${customStyles.agr_vertical_mid}`}>Emilia Targaryen</td>
                                <td className={`${customStyles.agr_vertical_mid} col-lg-6`}>
                                    <div>
                                        Emilia Targaryen is a hair perming specialist. She is able to perm and do rebonding person with curly structure hair to a straight structure hair. It alternate the protein structure of the hair
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src='https://cdn.discordapp.com/attachments/750970916456103956/881257483623084113/unknown.png' className={`${customStyles.fyp_circle} ${customStyles.fyp_100} ${customStyles.dropdownbtn}`}></img>
                                </td>
                                <td className={`${customStyles.agr_vertical_mid}`}>Olivia Joy</td>
                                <td className={`${customStyles.agr_vertical_mid} col-lg-6`}>
                                    <div>
                                        She's the best person for styling difficult hair customStyles for woman , got many awards from many different competition
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src='https://cdn.discordapp.com/attachments/750970916456103956/881239317048086578/unknown.png' className={`${customStyles.fyp_circle} ${customStyles.fyp_100} ${customStyles.dropdownbtn}`}></img>
                                </td>
                                <td className={`${customStyles.agr_vertical_mid}`}>Toon Say Jun</td>
                                <td className={`${customStyles.agr_vertical_mid} col-lg-6`}>
                                    <div>
                                        Toon Say Jun is our Co-founder of Hair Do Salon. He is the best in his field in applying aromatheraphy.Aromatherapy uses aromatic essential oils medicinally to improve the health of the body, mind, and spirit. It enhances both physical and emotional health
                                    </div>

                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <img src='https://cdn.discordapp.com/attachments/750970916456103956/881260836813930506/unknown.png' className={`${customStyles.fyp_circle} ${customStyles.fyp_100} ${customStyles.dropdownbtn}`}></img>
                                </td>
                                <td className={`${customStyles.agr_vertical_mid}`}>Zuri</td>
                                <td className={`${customStyles.agr_vertical_mid} col-lg-6`}>
                                    <div>
                                        Zuri is a junior associate in our firm that specialize in applying color that is requested by customer.She is able to mix and create colors perfectly that are requested by the customer.
                                    </div>

                                </td>
                            </tr>
                            {/* <tr>
                            <td>4</td>
                            // <td colSpan="2">Larry the Bird</td> 
                            <td>@twitter</td>
                            <td>@twitter</td>
                        </tr> */}
                        </tbody>
                    </Table>
                </div>
            </div>
        </div>
    )

}