import {
  Calendar,
  momentLocalizer,
  Views
} from "react-big-calendar";
import moment from "moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import fire from '../config/fire-conf';
import { useState, useEffect } from 'react';
import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CustomEvent from './CustomEvent';
import withDragAndDrop from 'react-big-calendar/lib/addons/dragAndDrop';
import customStyles from '../styles/Home.module.scss';
import Typography from '@material-ui/core/Typography'
import { makeStyles, responsiveFontSizes } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Alert, { AlertProps } from '@material-ui/lab/Alert';


moment.locale("en-GB");
//momentLocalizer(moment);
const localizer = momentLocalizer(moment);
const DragAndDropCalendar = withDragAndDrop(Calendar)

//for react-big-calendar (month view)
const styles = {
  container: {
    width: "80wh",
    height: "70vh",
    margin: "2em"
  }
};

const useStyles = makeStyles((theme) => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
    width: 'fit-content',
    fontSize: '20px',
  },
  formControl: {
    marginTop: theme.spacing(0),
    minWidth: 60,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
  textField: {
    fontWeight: 500,
    fontSize: '15px',
  }
}));

const generateEventId = (event) => {
  return event.start.toLocaleString() + event.end.toLocaleString();
};

export default function CustomCalendar({ email, displayName, isSeller }) {
  const updateEventNameInput = React.useRef(null);
  const updateEventNumPeopleInput = React.useRef(null);
  const updateEventContentInput = React.useRef(null);
  const classes = useStyles();

  //initial state
  const [events, setEvents] = useState([]);
  const [sellerEvents, setSellerEvents] = useState([]);
  const [content, setContent] = useState('');//description
  const [open, setOpen] = React.useState(false);//for add
  const [editOpen, setEditOpen] = React.useState(false);//for edit
  const [deleteOpen, setDeleteOpen] = React.useState(false);//for delete
  const [start, setStart] = useState(null);
  const [end, setEnd] = useState(null);
  const [name, setName] = useState('');//name
  const [num_people, setnumPeople] = useState(1);//numPeople // useState('')= string , (0) is for number and (true) is for boolean
  const [price, setPrice] = useState(10);
  const [draggedEvent, setDraggedEvent] = useState(null);
  const dragFromOutsideItem = () => {
    return draggedEvent;
  };
  const [isEdit, setEdit] = React.useState(false);//for editing mode  (boolean)
  const [clickedEvent, setClickedEvent] = useState({});
  const [initialFetch, setInitialFetch] = useState(true);
  const [snackbarEditSuccessOpen, setSnackbarEditSuccessOpen] = useState(false);
  const [snackbarEditOpen, setSnackbarEditOpen] = useState(false);
  const [snackbarAddOpen, setSnackbarAddOpen] = useState(false);
  const [snackbarDeleteOpen, setSnackbarDeleteOpen] = useState(false);

  const onEventDrop = ({
    event,
    start,
    end,
    isAllDay: droppedOnAllDaySlot
  }) => {
    if (!droppedOnAllDaySlot) {
      if (!event.disabled && !ifEventClashed({ start, end, id: event.id })) {
        const nextEvents = events.map((existingEvent) => {
          return existingEvent.id === event.id
            ? {
              ...existingEvent,
              id: generateEventId({ start, end }),
              start,
              end
            }
            : existingEvent;
        });
        setEvents(nextEvents);
      }
    }
  };
  const ifEventClashed = (event) => {
    for (const e of events) {
      if (event.id === e.id) {
        console.log("same");
        continue;
      }
      if ((event.start < e.start && event.end > e.start) || (event.start >= e.start && event.start < e.end)) {
        console.log(e);
        return true;
      }
    }
    return false;
  }
  // const datesAreOnSameDay = (date1, date2) => {
  //   return date1.getFullYear() === date2.getFullYear() &&
  //     date1.getMonth() === date2.getMonth() &&
  //     date1.getDate() === date2.getDate();
  // }
  const onEventResize = ({ event, start, end }) => {
    if ( !event.disabled && !ifEventClashed({ start, end, id: event.id })) {
      const nextEvents = events.map((existingEvent) => {
        return existingEvent.id === event.id
          ? {
            ...existingEvent,
            id: generateEventId({ start, end }),
            start,
            end,
          }
          : existingEvent;
      });
      setEvents(nextEvents);
    }
  };
  const onDropFromOutside = ({ start, end }) => {
    const event = {
      id: generateEventId({ start, end }),
      title: draggedEvent.title,
      start,
      end,
      num_people,
    };
    setDraggedEvent(null);
    onEventDrop({ event, start, end });
  };
  const handleDragStart = (event) => {
    setDraggedEvent(event);
  };
  const calculateHours = (start, end) => {
    return ((end - start) / 36e5);
  }

  //to get data //for seller

  useEffect(() => {
    console.log(isSeller);
    if (email) {

      fire.firestore()
        .collection('blog')
        .get().then(snap => {
          //create a document immediately with the current contents of the single document , Then each time content change, it updates 
          //snap = query snapshot = result of a query , docs = using foreach method 
          if (snap.docs) {
            // const events = snap.get('events').map((doc, i) => { //events is the name 
            //   // console.log(doc.get('start'))
            //   return {
            //     id: doc.id,
            //     title: doc.title,
            //     content: doc.content,
            //     num_people: doc.num_people,
            //     start: new Date(doc.start.toDate()),
            //     end: new Date(doc.end.toDate()),
            //   }
            // }
            // );
            // console.log(events);
            // setEvents(events);
            setEvents(snap.docs.map(doc => ({
              events: doc.data().events.map((e) => {
                console.log(doc.id)
                console.log(email)
                return {
                  ...e,
                  disabled: isSeller ? false : doc.id != email,
                  start: new Date(e.start.toDate()),
                  end: new Date(e.end.toDate())
                }
              }),

              email: doc.id,

            })).reduce((user1, user2) => {
              console.log(user2.events.length)

              if (user2.events.length > 0) {
                user1.push(...user2.events)

              }
              return user1
            }, []));

            setInitialFetch(false);
          }
          else {
            setInitialFetch(false);

          }
        })
    }
    // console.log(events);
  }, [isSeller, email])


  console.log(events)


  // useEffect(() => {
  //   console.log(isSeller);
  //   if (!isSeller) {
  //     console.log(email)
  //     if (email) {
  //       console.log(email)
  //       fire.firestore()
  //         .collection('blog')
  //         .doc(email)
  //         .get().then(snap => {
  //           //create a document immediately with the current contents of the single document , Then each time content change, it updates 
  //           //snap = query snapshot = result of a query , docs = using foreach method 
  //           if (snap.get('events')) {
  //             const events = snap.get('events').map((doc, i) => { //events is the name 
  //               // console.log(doc.get('start'))
  //               return {
  //                 id: doc.id,
  //                 title: doc.title,
  //                 content: doc.content,
  //                 num_people: doc.num_people,
  //                 start: new Date(doc.start.toDate()),
  //                 end: new Date(doc.end.toDate()),
  //               }
  //             }
  //             );
  //             console.log(events);
  //             setEvents(events);

  //             setInitialFetch(false);
  //           }
  //           else {
  //             setInitialFetch(false);

  //           }
  //         })
  //     }
  //   }
  // }, [isSeller, email])

  //email (edited) //for user
  // useEffect(() => {
  //   if (!isSeller) {
  //     if (email) {
  //       fire.firestore()
  //         .collection('blog')
  //         .doc(email)
  //         .get().then(snap => {
  //           //create a document immediately with the current contents of the single document , Then each time content change, it updates 
  //           //snap = query snapshot = result of a query , docs = using foreach method 
  //           if (snap.get('events')) {
  //             const events = snap.get('events').map((doc, i) => { //events is the name 
  //               // console.log(doc.get('start'))
  //               return {
  //                 id: doc.id,
  //                 title: doc.title,
  //                 content: doc.content,
  //                 num_people: doc.num_people,
  //                 start: new Date(doc.start.toDate()),
  //                 end: new Date(doc.end.toDate()),
  //               }
  //             }
  //             );
  //             console.log(events);
  //             setEvents(events);

  //             setInitialFetch(false);
  //           }
  //           else {
  //             setInitialFetch(false);

  //           }
  //         }, [email])
  //     }
  //   }
  // }, [events, isSeller])



  //to add events //for seller



  //  useEffect(() => {
  //   if (!initialFetch) {
  //     console.log(email)
  //     fire.firestore()
  //       .collection('users')
  //       .doc(email)
  //       .set({ events });
  //   }
  // }, [events, initialFetch])

  // useEffect(() => {
  //   if (!initialFetch) {
  //     console.log(email)
  //     fire.firestore()
  //       .collection('blog')
  //       .doc('calendarEvents')
  //       .set({ events });
  //   }
  // }, [events])




  //normal 
  useEffect(() => {
    if (email && !isSeller) {
      // if (email) { //original code
      if (!initialFetch) {
        console.log(email)
        fire.firestore()
          .collection('blog')
          .doc(email)
          .set({
            events: events.filter((e) => {
              return !e.disabled
            })
          });

      }
      
    }

  }, [email, events,isSeller])




  // const firebaseUpdateEvent = (events) => {
  //   events.preventDefault();
  //   fire.firestore()
  //     .collection('blog')
  //     .update({
  //       title: name,
  //       content,
  //       num_people,//if same name no need put num_people :num_people ( : semicolon)
  //       start,
  //       end,
  //     });
  //   // console.log('This event is updated!!');
  //   // console.log(events);
  // }


  const handleSubmit = (event) => {
    event.preventDefault();
    if (name && content && num_people) {
      if (!ifEventClashed({ start, end, id: event.id })) {
        setEvents([
          ...events,
          {
            id: generateEventId({ start, end }),
            start,
            end,
            title: name,  
            content,
            num_people,
          },
        ],

        )
      }

      // fire.firestore()
      //   .collection('blog')
      //   .doc('calendarEvents')
      //   .add({
      //     title: name,
      //     content,
      //     num_people,//if same name no need put num_people :num_people ( : semicolon)
      //     start,
      //     end,
      //   });
      // console.log(events);
      setName('');
      setContent('');
      setnumPeople(1);
      setOpen(false);
      setSnackbarAddOpen(true);
    }
    else {
      setSnackbarEditOpen(true);
 
    }

  }

  const updateEvent = () => {
    if (updateEventNameInput.current.value && updateEventContentInput.current.value && updateEventNumPeopleInput.current.value) {
      const nextEvents = events.map((existingEvent) => {//new array next events //map = check all element then modify the one needed
        return existingEvent.id === clickedEvent.id //for each element if it check same date and 
          ? {
            ...existingEvent,
            id: generateEventId({ start: clickedEvent.start, end: clickedEvent.end }),
            title: updateEventNameInput.current.value,
            content: updateEventContentInput.current.value,
            num_people: updateEventNumPeopleInput.current.value,
          }
          : existingEvent;
      });
      setEvents(nextEvents);//replacing events with the value of nextEvents
      setEditOpen(false);
      setSnackbarEditSuccessOpen(true);
    }
    //add snackbar
    else {
      // setSnackbarOpen(true);
      setSnackbarEditOpen(true);
    }
  }

  const deleteEvent = () => {
    const nextEvents = events.filter((existingEvent) => {//new array next events //map = check all element then modify the one needed
      return existingEvent.id !== clickedEvent.id //for each element if it check same date and 

    });
    setEvents(nextEvents);//replacing events with the value of nextEvents
    setDeleteOpen(false);
    setEditOpen(false);
    setSnackbarDeleteOpen(true);
  }
  const handleOpen = () => {
    setOpen(true);
  }
  const handleClose = () => {
    setOpen(false);
  };
  const handleEditOpen = () => {
    setEditOpen(true);
  }
  const handleEditClose = () => {
    setEditOpen(false);
  };
  const handleDeleteOpen = () => {
    setDeleteOpen(true);
  }

  const handleDeleteClose = () => {
    setDeleteOpen(false);
  };


  return (
    // {email},
    // console.log(email),
    <div style={styles.container}>
      {/* <p>{email}</p> */}
      {/* <p>{displayName}</p> */}
      <Dialog open={open} onClose={handleClose}>
        <DialogTitle>Add Schedule/ Reservation Time</DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            id="name"
            margin="dense"
            type="text"
            onChange={({ target }) => setName(target.value)}
            fullWidth
            required
            className={classes.formTextSize}
            label="Name"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          <TextField
            autoFocus
            id="content"
            margin="dense"
            label="Content"
            type="text"
            onChange={({ target }) => setContent(target.value)}
            fullWidth
            required
            InputLabelProps={{
              className: classes.textField,
            }}

          />
          {/* <DialogContentText>
            To subscribe to this website, please enter your email address here. We will send updates
            occasionally.
     </DialogContentText> */}
          <TextField
            autoFocus
            id="Num_People"
            margin="dense"
            label="Number of People"
            type="number"
            // onChange={({ target }) => setnumPeople(target.valueAsNumber)}
            value={num_people}
            onChange={({ target }) => setnumPeople(Math.max(1, Math.floor((target.valueAsNumber) ? Number(target.valueAsNumber) : target.valueAsNumber)))}
            InputProps={{ inputProps: { min: 1 } }}
            fullWidth
            required
            placeholder="1"
            InputLabelProps={{
              className: classes.textField,
            }}
          />

          <TextField

            margin="dense"

            // onChange={({ target }) => setPrice(target.value)}
            fullWidth
            // required
            disabled={true}
            value={calculateHours(start, end)}
            label="Number of Hours"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          <TextField

            margin="dense"

            // onChange={({ target }) => setPrice(target.value)}
            fullWidth
            // required
            disabled={true}
            value={price * calculateHours(start, end) * num_people}
            label="Price"
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <p>{price * calculateHours(start,end)}</p> */}
        </DialogContent>

        <DialogActions>
          <Button variant="outlined" onClick={(event) => { handleSubmit(event) }} color="primary">
            Book/Schedule
         </Button>
        </DialogActions>
      </Dialog>
      <Dialog
        open={editOpen}
        onClose={handleEditClose}
        aria-labelledby="handleSubmit"

      >
        <DialogTitle id="handleSubmit">Edit Schedule/ Reservation Time</DialogTitle>
        <DialogContent>
          {/* <DialogContent> */}
          {/* <DialogContentText>
            <strong>Name</strong>
          </DialogContentText> */}
          {/* </DialogContent> */}
          {/* <InputLabel htmlFor="Name">Name</InputLabel> */}
          <TextField
            autoFocus
            id="Name"
            margin="dense"
            type="text"
            defaultValue={clickedEvent.title}
            fullWidth
            inputRef={updateEventNameInput}
            onChange={({ target }) => setName(target.value)}
            required
            label="Name"
            InputLabelProps={{
              className: classes.textField,
            }}

          />
          {/* <InputLabel htmlFor="content">Content</InputLabel> */}
          <TextField
            autoFocus
            id="content"
            margin="dense"
            type="text"
            defaultValue={clickedEvent.content}
            fullWidth
            required
            inputRef={updateEventContentInput}
            label="Content"
            InputLabelProps={{
              className: classes.textField,
            }}
          // onChange={({ target }) => setContent(target.value)}
          />
          {/* <DialogContentText>
            <strong>Number of People</strong>
          </DialogContentText> */}
          {/* <InputLabel htmlFor="Num_people">Number of People</InputLabel> */}
          <TextField
            autoFocus
            id="Num_People"
            margin="dense"
            type="number"
            defaultValue={clickedEvent.num_people}
            value={num_people}
            InputProps={{ inputProps: { min: 1 } }} //input props for minimum value= 1 
            fullWidth
            required
            label="Number of People"
            inputRef={updateEventNumPeopleInput}
            onChange={({ target }) => setnumPeople(Math.max(1, Math.floor((target.valueAsNumber) ? Number(target.valueAsNumber) : target.valueAsNumber)))}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <InputLabel htmlFor="num_hours">Number of Hours</InputLabel> */}
          <TextField

            id="num_hours"
            margin="dense"
            // type="text"
            // onChange={({ target }) => setPrice(target.value)}
            fullWidth
            // required
            value={calculateHours(start, end)}
            //  defaultValue={price*calculateHours(start,end)}
            // variant="filled"
            disabled="true"
            label="Number of Hours"
            InputProps={{
              readOnly: true,
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <DialogContentText>
            <strong>Price</strong>
          </DialogContentText>
          <DialogContentText>
            Price
          </DialogContentText> */}
          {/* <InputLabel htmlFor="price">Price</InputLabel> */}
          <TextField

            id="price"
            margin="dense"
            // type="text"
            // onChange={({ target }) => setPrice(target.value)}
            fullWidth
            // required
            value={price * calculateHours(start, end) * num_people}
            //  defaultValue={price*calculateHours(start,end)}
            // variant="filled"
            disabled="true"
            label="Price"
            InputProps={{
              readOnly: true,
            }}
            InputLabelProps={{
              className: classes.textField,
            }}
          />
          {/* <p>{price * calculateHours(start,end)}</p> */}
        </DialogContent>

        <DialogActions>
          <Button variant="outlined" onClick={() => { updateEvent() }} color="primary">
            Update Reservation
         </Button>
          <Button variant="outlined" onClick={handleDeleteOpen} className={customStyles.error}>
            Delete Reservation
         </Button>
        </DialogActions>

      </Dialog>
      <Dialog
        open={deleteOpen}
        onClose={handleDeleteClose}
        aria-labelledby="handleSubmit"
      // fullWidth={fullWidth}
      // maxWidth= {maxWidth}
      >
        <DialogTitle>Are you sure you want to delete?</DialogTitle>
        <DialogActions>
          <Button variant="outlined" onClick={() => { deleteEvent() }} color="primary">
            Yes
         </Button>
          <Button variant="outlined" onClick={handleDeleteClose} className={customStyles.error}>
            No
         </Button>
        </DialogActions>
      </Dialog>

      <DragAndDropCalendar
        selectable="ignoreEvents"
        localizer={localizer}
        onEventDrop={onEventDrop}
        onEventResize={onEventResize}
        dragFromOutsideItem={dragFromOutsideItem}
        onDropFromOutside={onDropFromOutside}
        handleDragStart={handleDragStart}
        resizable
        events={events}
        defaultView={Views.WEEK}
        views={[Views.DAY, Views.WEEK, Views.MONTH]}
        steps={60}
        defaultDate={new Date(2021, 0, 30)} // January is 0 , december is 11 // being counted like that
        // onSelectEvent={events =>alert(events.title) }
        onSelectEvent={event => {
          // updateEvent(events);
          if (event.disabled ){
            return ;
          }
            handleEditOpen();
          // handleDeleteOpen();
          // updateEvent(event);//to check for console log
          setClickedEvent(event);
          setnumPeople(event.num_people);
          setStart(event.start);
          setEnd(event.end);
          // console.log(events.title);
        }
        }
        onSelectSlot={({ start, end, num_people }) => {
          handleOpen();
          // console.log(open);
          setStart(start);
          setEnd(end);
        }}
        components={{
          event: CustomEvent,
        }}
        timeslots={2}//custom time grid for timeslots , 
        min={new Date(2017, 0, 27, 10, 0, 0)}
        max={new Date(2017, 0, 27, 22, 0, 0)}
        eventPropGetter={(event) => {
          return {
            style: {
              backgroundColor: event.disabled ? 'grey' : '#112fd6',
              opacity: event.disabled ? '0.5' : '1',
              cursor: event.disabled ? 'no-drop' : 'grab',
            }
            
          }
        }}


      />
      {/*Snack Bar Edit */}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 300,
          height: 200,
          flexGrow: 0,
        }}
        open={snackbarEditOpen}
        onClose={() => {
          setSnackbarEditOpen(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          // backgroundColor: 'teal',
        }}
          // message={<span style={{
          //   fontSize: '15px',
          // }}>Please fill in all details</span>}
          severity='warning'
        >
          <span style={{
            fontSize: '15px',
          }}>Please fill in all details</span>
        </Alert>
      </Snackbar>
      {/* Snackbar Delete */}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 200,
          height: 200,
          flexGrow: 0,
        }}
        open={snackbarDeleteOpen}
        onClose={() => {
          setSnackbarDeleteOpen(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          // backgroundColor: '#f44336',
        }}
          // message={<span style={{
          //   fontSize: '20px',
          // }}>Successfully Deleted</span>}
          severity='error'
        ><span style={{
          fontSize: '20px',
        }}>Successfully Deleted</span>
        </Alert>
      </Snackbar>
      {/* Snackbar Add */}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 200,
          height: 200,
          flexGrow: 0,
        }}
        open={snackbarAddOpen}
        onClose={() => {
          setSnackbarAddOpen(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          // backgroundColor: '#f44336',
        }}
          // message={<span style={{
          //   fontSize: '20px',
          // }}>Successfully Deleted</span>}
          severity='success'
        ><span style={{
          fontSize: '20px',
        }}>Successfully Added</span>
        </Alert>
      </Snackbar>
      {/* Snackbar Edit Success */}
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'center',
        }}
        bodyStyle={{
          width: 200,
          height: 200,
          flexGrow: 0,
        }}
        open={snackbarEditSuccessOpen}
        onClose={() => {
          setSnackbarEditSuccessOpen(false)
        }

        }
        autoHideDuration={3000}
      >
        <Alert style={{
          // backgroundColor: '#f44336',
        }}
          // message={<span style={{
          //   fontSize: '20px',
          // }}>Successfully Deleted</span>}
          severity='success'
        ><span style={{
          fontSize: '20px',
        }}>Successfully Edited</span>
        </Alert>
      </Snackbar>
    </div>
  );
}
