import styles from '../styles/Home.module.scss';
import fire from '../config/fire-conf';
import { useState, useEffect, useRef } from 'react';
import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import Alert, { AlertProps } from '@material-ui/lab/Alert';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

export default function AddStaff({ newStaffHandler }) {
    const inputStaffRef = useRef(null);
    const inputStaffEmailRef = useRef(null);
    const [newStaff, setNewStaff] = useState('');
    const [newStaffEmail, setNewStaffEmail] = useState('');
    const [initialFetch, setInitialFetch] = useState(true);
    const [serviceBusinessName, setServiceBusinessName] = useState([]);
    const [snackbarEditSuccessOpen, setSnackbarEditSuccessOpen] = useState(false);
    const [serviceBusiness, setServiceBusiness] = useState([]);
    const [snackbarEditOpen, setSnackbarEditOpen] = useState(false);
    const [snackbarDeleteOpen, setSnackbarDeleteOpen] = useState(false);
    const [deleteOpen, setDeleteOpen] = React.useState(false);//for delete

    //get Staff Data
    useEffect(() => {
        fire.firestore()
            .collection('salon')
            .get().then(snap => {
                if (snap.docs) {
                    setServiceBusinessName(snap.docs.map((doc, i) => ({
                        name: doc.id,
                    })));
                    setInitialFetch(false);
                }
                else {
                    setInitialFetch(false);
                }
            })

    }, [])
    //set Staff Data
    useEffect(() => {
        if (inputStaffRef.current && newStaff) {
            fire.firestore()
                .collection('salon')
                .doc(newStaff)
                .set(
                    {
                        email: [],
                    }
                )

            inputStaffRef.current.value = '';

            setSnackbarEditSuccessOpen(true);
        }
        else {
            setSnackbarEditOpen(true);
            // console.log(snackbarEditOpen)
        }
    }, [newStaff])

    useEffect(() => {
        if (inputStaffEmailRef.current && newStaffEmail) {
            fire.firestore()
                .collection('userData')
                .doc(newStaffEmail)
                .set(
                    {
                        seller: true,
                    }
                )

            inputStaffEmailRef.current.value = '';

            setSnackbarEditSuccessOpen(true);
        }
        else {
            setSnackbarEditOpen(true);
            // console.log(snackbarEditOpen)
        }
    }, [newStaffEmail])
    
    // console.log(serviceBusinessName);
    return (

        <div className="container">

            <div className={`row`}>
                <div className={`col-lg-6`}> 
                    <h3>Add Staff</h3>

                    {/* <form onSubmit={handleAddStaff}> */}
                    <div className={`${styles.agr_margin_between_row}`}>
                        <input ref={inputStaffRef} id="newStaff" className={`${styles.fyp_login_text} ${styles.fyp_destroy_margin_side} ${styles.agr_width_sixty}`} type="text" required placeholder="Add Staff Name" />
                    </div>
                    <button onClick={() => {
                        setNewStaff(inputStaffRef.current.value)
                        newStaffHandler();
                    }} className={`${styles.fyp_get_started_button}`}>Add Staff</button>
                </div>
            </div>

            <div className={`row`}>
                <div className={`col-lg-6`}>
                    <h3>Add Staff Authorization</h3>

                    {/* <form onSubmit={handleAddStaff}> */}
                    <div className={`${styles.agr_margin_between_row}`}>
                        <input ref={inputStaffEmailRef} id="newStaffAuthorization" className={`${styles.fyp_login_text} ${styles.fyp_destroy_margin_side} ${styles.agr_width_sixty}`} type="text" required placeholder="Add Staff Authorization" />
                    </div>
                    <button onClick={() => {
                        setNewStaffEmail(inputStaffEmailRef.current.value)
                        newStaffHandler();
                    }} className={`${styles.fyp_get_started_button}`}>Add Staff Email Authorization</button>
                </div>
            </div>
            <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                bodyStyle={{
                    width: 200,
                    height: 200,
                    flexGrow: 0,
                }}
                open={snackbarEditSuccessOpen}
                onClose={() => {
                    setSnackbarEditSuccessOpen(false)
                }

                }
                autoHideDuration={3000}
            >
                <Alert style={{
                    // backgroundColor: '#f44336',
                }}
                    // message={<span style={{
                    //   fontSize: '20px',
                    // }}>Successfully Deleted</span>}
                    severity='success'
                ><span style={{
                    fontSize: '20px',
                }}>Successfully Added Staff</span>
                </Alert>
            </Snackbar>
        </div >
    )
}