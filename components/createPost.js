// // components/CreatePost.js
// import React, { useState } from 'react';
// import fire from '../config/fire-conf';

// const CreatePost = () => {
//   var date = new Date();
//   const [events, setEvents] = useState([]);
//   const [name, setName] = useState('');
//   const [content, setContent] = useState('');
//   const [scheduleDateFirebase, setScheduleDateFirebase] = useState(date);
//   const [scheduleDate, setScheduleDate] = useState('');
//   const [scheduleTime, setScheduleTime] = useState('');
//   const [num_people, setnumPeople] = useState(0); // useState('')= string , (0) is for number and (true) is for boolean
//   const [notification, setNotification] = useState('');
//   const handleSubmit = (event) => {
//     event.preventDefault();
//     fire.firestore()
//       .collection('blog')
//       .add({
//         name: name,
//         content: content,
//         num_people: num_people,
//         scheduleDateFirebase: scheduleDateFirebase,
//         scheduleTime: scheduleTime,
//       });
//     setName('');
//     setContent('');
//     setScheduleDateFirebase(date);
//     setScheduleDate('');
//     setScheduleTime('');
//     setnumPeople(0);
//     setNotification('Schedule created');
//     setTimeout(() => {
//       setNotification('')
//     }, 2000)
//   }
//   return (
//     <div>
//       <h2>Add Schedule Date/Time</h2>
//       {notification}
//       <form onSubmit={handleSubmit}>
//         <div>
//           <label for="name">Name:</label><br />
//           <input type="text" value={name}
//             onChange={({ target }) => setName(target.value)} />
//         </div>
//         <div>
//           <label for="description">Description:</label><br />
//           <input type="text" value={content}
//             onChange={({ target }) => setContent(target.value)} />
//         </div>
//         <div>
//           <label for="num_people">Number of people:</label><br />
//           <input type="number" value={num_people}
//             onChange={({ target }) => setnumPeople(target.valueAsNumber)} min="0" />

//         </div>
//         <div>
//           <label for="scheduleDate">Schedule Date</label><br />
//           <input type="date" value={scheduleDate}
//             onChange={({ target }) => {
//               console.log(target.value);
//               setScheduleDate(target.value)
//               setScheduleDateFirebase(target.valueAsDate)
//             }

//             } />

//         </div>
//         <div>
//           <label for="scheduleTime">Schedule Time</label><br />
//           <input type="time" value={scheduleTime}
//             onChange={({ target }) => {
//               console.log(target.value)
//               setScheduleTime(target.value)
//             }

//             } />

//         </div>

//         <button type="submit">Save</button>
//       </form>
//     </div>
//   )
// }
// export default CreatePost;