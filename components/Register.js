import { useState } from 'react';
import fire from '../config/fire-conf';
import { useRouter } from 'next/router'

import styles from '../styles/Home.module.scss'

export default function Register() {
  const router = useRouter();

  const [userName, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [passConf, setPassConf] = useState('');

  const [notify, setNotification] = useState('');
  const facebookProvider = new fire.auth.FacebookAuthProvider();
  const googleProvider = new fire.auth.GoogleAuthProvider();

  const handleLogin = (e) => {
    e.preventDefault();

    if (password !== passConf || password.length <= 6) {
      setNotification('Password and password confirmation does not match , Minumum require is 7 characters')

      setTimeout(() => {
        setNotification('')
      }, 4000)

      setPassword('');
      setPassConf('');
      return null;

    }

    fire.auth()
      .createUserWithEmailAndPassword(userName, password)
      .catch((err) => {
        console.log(err.code, err.message)
      });

    router.push("/")
  }

  return (
    <div>
      <div>


        <h1 className={`${styles.fyp_text_align_center}` }> Create new user</h1>
        <div className={`${styles.fyp_display_flex}`}>
          <div className={`${styles.fyp_login} ${styles.fyp_flex_auto} ${styles.fyp_margin_side_15}`}>
            {notify}

            <form onSubmit={handleLogin}>
              <div className={`${styles.agr_margin_between_row}`}>
                {/* <label htmlFor="Email">Email </label> */}
                <input className={`${styles.fyp_login_text}`} type="email" value={userName} onChange={({ target }) => setUsername(target.value)} required placeholder="Email Address" />
              </div>


              <div className={`${styles.agr_margin_between_row}`}>
                {/* <label htmlFor="Password">Password </label> */}
                <input className={`${styles.fyp_login_text}`} type="password" value={password} onChange={({ target }) => setPassword(target.value)} placeholder="Minimum 7 character" required />
              </div>

              <div className={`${styles.agr_margin_last_row}`}>
                {/* <label htmlFor="Password">Password confirmation </label><br /> */}
                <input className={`${styles.fyp_login_text}`} type="password" value={passConf} onChange={({ target }) => setPassConf(target.value)} placeholder="Please retype password " required />
              </div>

              <div className={`${styles.login_register_button}`}>
                <button type="submit" className={`${styles.agr_margin_last_row}  ${styles.login_register_button}`}>
                  <img src="https://i.pinimg.com/originals/8f/c3/7b/8fc37b74b608a622588fbaa361485f32.png" alt="Facebook"></img>

                  <span>Register With Email and Password</span>
                </button>

              </div>
            </form>
            <div className={`${styles.login_register_button}`}>
              <hr />
              <button
                onClick={() => {
                  const facebookProvider = new fire.auth.FacebookAuthProvider();

                  fire.auth().signInWithPopup(facebookProvider);
                  // console.log(facebookProvider);
                  //sign up with Facebook
                  setTimeout(() => {
                    router.push("/")

                  }, 3000)
                }}
                className={`${styles.agr_margin_last_row} `}>
                <img src="https://p.kindpng.com/picc/s/736-7363083_download-facebook-logo-png-icon-high-quality-transparent.png" alt="Facebook" width='30px' height='30px'></img>
                {' '}<span>Register with Facebook</span>

              </button>
            </div>
            <div className={`${styles.login_register_button}`}>
              <button
                onClick={() => {
                  const googleProvider = new fire.auth.GoogleAuthProvider();

                  fire.auth().signInWithPopup(googleProvider);
                  // console.log(googleProvider);
                  //sign up with Google
                  setTimeout(() => {
                    router.push("/")

                  }, 3000)
                }}
                className={`${styles.agr_margin_last_row}`}>
                <img src="https://image.similarpng.com/thumbnail/2020/12/Colorful-google-logo-design-on-transparent-PNG-1.png" alt="Facebook" width='30px' height='30px'></img>

                <span>Register with Google</span>

              </button>
              {/* <GoogleLogin
              clientId="303780915676-lrgjad8csk2i519fc382fhdmd9janf59.apps.googleusercontent.com"
              buttonText="Login With Google"
              onSuccess={responseGoogleSuccess}
              onFailure={responseGoogleFail}
              cookiePolicy={'single_host_origin'}
            >
            </GoogleLogin> */}
            </div>


          </div>
        </div>
      </div>
    </div >
  )

}