import { useState, useEffect } from 'react';
import { fire, storage } from '../config/fire-conf';
import { useRouter } from 'next/router'
import styles from '../styles/Home.module.scss';
// import EditProfile from '../pages/users/EditProfile';
import Link from 'next/link';

export default function ViewProfile({ photoURL, displayName, email }) {
    // const allInputs = { imgUrl: '' }
    // const [file, setFile] = useState(null);
    // const [url, setURL] = useState("");
    // console.log(file)
    // function handleChange(e) {
    //     setFile(e.target.files[0]);
    // }
    // function handleUpload(e) {
    //     e.preventDefault();
    //     const ref = storage.ref(`/images/${file.name}`);
    //     const uploadTask = ref.put(file);
    //     uploadTask.on("state_changed", console.log, console.error, () => {
    //         ref
    //             .getDownloadURL()
    //             .then((url) => {
    //                 setFile(null);
    //                 setURL(url);
    //             });
    //     });
    // }
    // console.log(url);

    return (
        // <div className={`${styles.fyp_display_flex} ${styles.agr_margin_top_6}`}>
        <div className={`${styles.agr_margin_top_6}`}>
            {/* <div className={`col-lg-2 ${styles.fyp_js_tabs_border} ${styles.agr_margin_left_10}`}> */}
            <div className={`${styles.fyp_padding_10} col-lg-3 ${styles.fyp_destroy_padding_right}`}>
                <div className={` ${styles.fyp_js_tabs_border} ${styles.fyp_border_radius_10}`}>
                    <div className={`${styles.title}`}>
                        <h3><strong>My Profile</strong></h3>
                        <div className={`${styles.justify_content_center} ${styles.agr_margin_last_row} ${styles.fontSize_initial} ${styles.fyp_padding_all_10}`}>
                            <div className={`${styles.fyp_border_radius_10} ${styles.fyp_padding_10} `}>
                                <div className={` ${styles.fyp_display_flex} ${styles.justify_content_center}`}>
                                    <div className={`${styles.fyp_login_header} ${styles.fyp_display_flex} ${styles.enlarge} ${styles.fyp_padding_all_10}  ${styles.fyp_border_radius_10}`}>
                                        {photoURL
                                            ?
                                            <div className={`${styles.enlarge} ${styles.fyp_display_flex} ${styles.alignItemMIddle}`}>
                                                <img src={photoURL} alt={displayName} className={`${styles.fyp_circle}`} ></img>
                                            </div>
                                            :
                                            <div className={`${styles.enlarge}  ${styles.fyp_display_flex}   ${styles.alignItemMIddle}`}>
                                                <img src="https://w7.pngwing.com/pngs/696/429/png-transparent-computer-icons-user-account-colombo-filippetti-spa-human-icon-share-icon-artwork-user-account-thumbnail.png" alt={displayName} className={`${styles.fyp_circle} ${styles.fyp_100}`} ></img>
                                            </div>
                                        }
                                        {/* <div>
                            <form onSubmit={handleUpload}>
                                <input type="file" onChange={handleChange} />
                                <button disabled={!file}>upload to firebase</button>
                            </form>
                            <img src={url} alt="" className={`${styles.fyp_50} ${styles.fyp_circle}`} />
                        </div> */}
                                        <div className={`${styles.self_align_center} ${styles.agr_margin_left_15} ${styles.enlarge} ${styles.fyp_display_flex}  ${styles.alignItemMIddle}`}>
                                            {/* <div className={`row`}> */}
                                            <div className={``}>
                                                Name: {displayName}
                                                <br />
                                                Email : {email}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <div className={`${styles.justify_content_center} ${styles.agr_margin_last_row} ${styles.fyp_display_flex}`}>

                    <div className={`row`}>
                        <Link href="./users/EditProfile">
                            <button className={`${styles.purple_color} ${styles.fyp_border_radius_10}`}>
                                <img src="https://w7.pngwing.com/pngs/696/429/png-transparent-computer-icons-user-account-colombo-filippetti-spa-human-icon-share-icon-artwork-user-account-thumbnail.png" className={`${styles.fyp_circle} ${styles.fyp_35}`}></img>

                                <span className={`${styles.text_white} ${styles.fyp_flex_auto} ${styles.agr_margin_left_5} ${styles.self_align_center}`}><strong>Edit Profile</strong></span>
                            </button>
                        </Link>
                    </div>
                </div> */}
                </div>
            </div>
            <div className={`${styles.fyp_padding_10} col-lg-9 `}>
                <div className={` ${styles.fyp_js_tabs_border} ${styles.fyp_551} ${styles.fyp_border_radius_10}`}>
                    <div className={`${styles.title}`}>
                        <h3><strong>Our Store Location</strong></h3>
                        <div className={`${styles.justify_content_center} ${styles.agr_margin_last_row} ${styles.fontSize_initial}`}>
                            <div className={`${styles.fyp_border_radius_10} ${styles.fyp_padding_10} `}>
                                <div className={`${styles.justify_content_center}`}>
                                    <div className={`col-lg-4  ${styles.fyp_padding_all_10} `}>
                                        <div className={`${styles.fyp_login_header}  ${styles.fyp_display_flex} ${styles.enlarge} ${styles.fyp_border_radius_10} ${styles.fyp_padding_all_10}`}>
                                            <div className={`${styles.self_align_center} ${styles.agr_margin_left_15} `}>

                                                <img src="https://cdn.discordapp.com/attachments/881360432655388672/881360533687763074/asgard.png" className={`${styles.fyp_circle} ${styles.fyp_100}`}></img>

                                                <div>Location : Asgard of Midgard of Son of Thor , Odinson , Loki , Asgard is not the place , its the people , 43040, Asgard</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={`col-lg-4  ${styles.fyp_padding_all_10} `}>
                                        <div className={`${styles.fyp_login_header}  ${styles.fyp_display_flex}  ${styles.enlarge} ${styles.fyp_border_radius_10} ${styles.fyp_padding_all_10}`}>
                                            <div className={`${styles.self_align_center} ${styles.agr_margin_left_15}  `}>
                                                <img src="https://cdn.discordapp.com/attachments/881360432655388672/881360600624664596/quantumRealm.jpg" className={`${styles.fyp_circle} ${styles.fyp_100}`}></img>
                                                <div>Location : 60, Jln AntmanWasp , Bandar Time works differently here, 444000, Quantum</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={`col-lg-4  ${styles.fyp_padding_all_10} `}>
                                        <div className={`${styles.fyp_login_header}  ${styles.fyp_display_flex}  ${styles.enlarge} ${styles.fyp_border_radius_10} ${styles.fyp_padding_all_10}`}>
                                            <div className={`${styles.self_align_center} ${styles.agr_margin_left_15} `}>
                                                <img src="https://cdn.discordapp.com/attachments/881360432655388672/881360613249540126/saaa.png" className={`${styles.fyp_circle} ${styles.fyp_100}`}></img>
                                                <div>Location : Sakaar,  , Jalan Thor and Hulk, City of GrandMaster & Valkrie , 122200 , Sakkaaarrr </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={`col-lg-4  ${styles.fyp_padding_all_10} `}>
                                        <div className={`${styles.fyp_login_header}  ${styles.fyp_display_flex}  ${styles.enlarge} ${styles.fyp_border_radius_10} ${styles.fyp_padding_all_10}`}>
                                            <div className={`${styles.self_align_center} ${styles.agr_margin_left_15}  `}>
                                                <img src="https://cdn.discordapp.com/attachments/881360432655388672/881360632543330334/titan.png" className={`${styles.fyp_circle} ${styles.fyp_100}`}></img>
                                                <div>Location: The desolated home planet , Titans , Galaxy 1113 </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={`col-lg-4  ${styles.fyp_padding_all_10} `}>
                                        <div className={`${styles.fyp_login_header}  ${styles.fyp_display_flex}  ${styles.enlarge} ${styles.fyp_border_radius_10} ${styles.fyp_padding_all_10}`}>
                                            <div className={`${styles.self_align_center} ${styles.agr_margin_left_15}  `}>

                                                <img src="https://cdn.discordapp.com/attachments/881360432655388672/881360562406178867/AboutUsBackground.png" className={`${styles.fyp_circle} ${styles.fyp_100}`}></img>
                                                <div>Location: Fairfax House, 15 Fulwood Pl, London WC1V 6AY, United Kingdom</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div >
    )

}