export default function CustomEvent({ event }) {

    return (
        <div >
            {!event.disabled && <>
            <div>Name : </div>
            <div>
                {event.title}
            </div>
            <br />
            <div>Content:</div>
            <div>
                {event.content}
            </div>
            <br />
            <div>Number of People</div>
            <div>
                {event.num_people}
            </div>
            <br />
            </>}
        </div>

    )
}